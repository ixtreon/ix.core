using Ix.Core.Memory;
using NUnit.Framework;
using System;
using System.Linq;

namespace Tests
{
    public class RefListTests
    {

        readonly RefList<int> theList = new RefList<int>(Enumerable.Range(0, 10));

        //[SetUp]
        //public void Setup()
        //{
        //}

        [Test]
        public void MoveElement()
        {
            // initial conditions
            Assert.IsTrue(theList.SequenceEqual(new[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }));

            // same-position moves
            theList.MoveElement(0, 0);
            Assert.IsTrue(theList.SequenceEqual(new[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }));

            theList.MoveElement(4, 4);
            Assert.IsTrue(theList.SequenceEqual(new[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }));

            theList.MoveElement(9, 9);
            Assert.IsTrue(theList.SequenceEqual(new[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }));

            // move forwards
            theList.MoveElement(0, 3);
            Assert.IsTrue(theList.SequenceEqual(new[] { 1, 2, 3, 0, 4, 5, 6, 7, 8, 9 }));

            // move backwards
            theList.MoveElement(7, 3);
            Assert.IsTrue(theList.SequenceEqual(new[] { 1, 2, 3, 7, 0, 4, 5, 6, 8, 9 }));

            // swap 2 numbers
            theList.MoveElement(8, 9);
            Assert.IsTrue(theList.SequenceEqual(new[] { 1, 2, 3, 7, 0, 4, 5, 6, 9, 8 }));
            theList.MoveElement(8, 9);
            Assert.IsTrue(theList.SequenceEqual(new[] { 1, 2, 3, 7, 0, 4, 5, 6, 8, 9 }));
        }

        [Test]
        public void MoveElement_OutOfRange()
        {
            Assert.Throws<IndexOutOfRangeException>(() => theList.MoveElement(-1, 3));
            Assert.Throws<IndexOutOfRangeException>(() => theList.MoveElement(10, 3));

            Assert.Throws<IndexOutOfRangeException>(() => theList.MoveElement(3, -1));
            Assert.Throws<IndexOutOfRangeException>(() => theList.MoveElement(3, 10));
        }

    }
}