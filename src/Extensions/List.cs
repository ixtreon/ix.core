﻿using Ix.Core.Memory;

namespace System.Collections.Generic
{
    public static class Extensions
    {
        /// <summary>
        /// Removes and returns the last element of the list. 
        /// </summary>
        public static T Pop<T>(this List<T> l)
        {
            var id = l.Count - 1;
            var elem = l[id];
            l.RemoveAt(id);
            return elem;
        }

        /// <summary>
        /// Removes and returns the last element of the list. 
        /// </summary>
        public static T RemoveLast<T>(this List<T> l) 
            => l.Pop();

        /// <summary>
        /// Removes a given element of the list by moving the last item in its place.
        /// <para/>
        /// Handy for manual removal of items using i++ loops - but don't forget to iterate the same i again!
        /// </summary>
        public static void RemoveAtFast<T>(this IList<T> l, int id)
        {
            var lastId = l.Count - 1;
            if (id < lastId)
                l[id] = l[lastId];
            l.RemoveAt(lastId);
        }


        public static int BinarySearch<T>(this IList<T> keys, T value)
            where T : IComparable<T>
        {
            var min = 0;
            var max = keys.Count - 1;
            while (min <= max)
            {
                var mid = (max + min) / 2;
                var cmp = keys[mid].CompareTo(value);
                if (cmp == 0)
                    return mid;

                (min, max) = (cmp < 0) ? (min, mid - 1) : (mid + 1, max);
            }

            return ~min;
        }

        public static int BinarySearch(this IList<string> keys, ReadOnlySpan<char> value, StringComparison comparer)
        {
            var min = 0;
            var max = keys.Count - 1;
            while (min <= max)
            {
                var mid = (max + min) / 2;
                var cmp = value.CompareTo(keys[mid], comparer);
                if (cmp == 0)
                    return mid;

                (min, max) = (cmp < 0) ? (min, mid - 1) : (mid + 1, max);
            }

            return ~min;
        }

        public static int BinarySearch<T>(this IList<KeyValuePair<string, T>> source, ReadOnlySpan<char> value, StringComparison comparer)
        {
            var min = 0;
            var max = source.Count - 1;
            while (min <= max)
            {
                var mid = (max + min) / 2;
                var cmp = value.CompareTo(source[mid].Key, comparer);
                if (cmp == 0)
                    return mid;

                (min, max) = (cmp < 0) ? (min, mid - 1) : (mid + 1, max);
            }

            return ~min;
        }
    }
}
