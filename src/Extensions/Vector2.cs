﻿using Ix.Math;

namespace System.Numerics
{

    public static class Vector2Extensions
    {
        /// <summary>
        /// Calculates the angle from the origin to this vector.
        /// </summary>
        public static float Angle(this in Vector2 v)
            => (float)Math.Atan2(v.Y, v.X);

        /// <summary>
        /// Calculates the angle from this vector to the given vector.
        /// </summary>
        public static float AngleTo(this in Vector2 v, in Vector2 other)
            => (other - v).Angle();

        /// <summary>
        /// Calculates the distance between the points represented by the two vectors.
        /// </summary>
        public static float DistanceTo(this in Vector2 a, in Vector2 b)
            => Vector2.Distance(a, b);

        /// <summary>
        /// Calculates the squared distance between the points represented by the two vectors.
        /// A bit quicker than <see cref="DistanceTo(Vector2, Vector2)"/>.
        /// </summary>
        public static float DistanceToSquared(this in Vector2 a, in Vector2 b)
            => Vector2.DistanceSquared(a, b);

        /// <summary>
        /// Calculates the distance between this point 
        /// and the closest point within the given rectangle.
        /// </summary>
        public static float DistanceTo(this Vector2 a, in RectangleF rect)
            => Vector2.Distance(a, a.Clamp(rect));

        /// <summary>
        /// Calculates the squared distance between this point 
        /// and the closest point within the given rectangle.
        /// A bit quicker than <see cref="DistanceTo(Vector2, RectangleF)"/>.
        /// </summary>
        public static float DistanceToSquared(this Vector2 a, in RectangleF rect)
            => Vector2.DistanceSquared(a, a.Clamp(rect));

        /// <summary>
        /// Gets the L1 norm of this vector, i.e. <see cref="X"/> + <see cref="Y"/>. 
        /// </summary>
        public static float NormL1(this in Vector2 v)
        {
            var vAbs = Vector2.Abs(v);
            return vAbs.X + vAbs.Y;
        }

        /// <summary>
        /// Calculates the L2 norm of this vector, i.e. its <see cref="Vector2.Length"/>. 
        /// </summary>
        public static float NormL2(this in Vector2 v)
            => v.Length();

        /// <summary>
        /// Offsets this vector by a distance in the given direction.
        /// </summary>
        [Obsolete]
        public static Vector2 PolarProjection(this in Vector2 v, float radians, float distance)
            => v.OffsetAtAngle(radians, distance);

        public static Vector2 Normalize(this in Vector2 v) 
            => v / v.Length();

        /// <summary>
        /// Offsets this vector by a distance in the given direction.
        /// </summary>
        public static Vector2 OffsetAtAngle(this in Vector2 v, float radians, float distance)
            => v + new Vector2(distance, 0).RotateAroundZero(radians);

        /// <summary>
        /// Rotates this vector around the given pivot.
        /// </summary>
        public static Vector2 RotateAround(this in Vector2 v, in Vector2 pivot, float radians)
            => pivot + (v - pivot).RotateAroundZero(radians);


        /// <summary>
        /// Rotates this vector around <see cref="Vector2.Zero"/>.
        /// </summary>
        public static Vector2 RotateAroundZero(this in Vector2 v, float radians)
            => Vector2.Transform(v, Matrix3x2.CreateRotation(radians));

        /// <summary>
        /// Rotates this vector around the origin.
        /// </summary>
        public static Vector2 Rotate(this in Vector2 v, float radians)
            => Vector2.Transform(v, Matrix3x2.CreateRotation(radians));

        public static Vector2 Clamp(this in Vector2 v, in Vector2 min, in Vector2 max)
            => Vector2.Clamp(v, min, max);

        public static Vector2 Clamp(this in Vector2 v, in RectangleF rect)
            => Vector2.Clamp(v, rect.Position, rect.FarPosition);

        public static bool IsInside(this in Vector2 v, in Vector2 pos, in Vector2 size)
            => v.Clamp(pos, pos + size) == v;

        public static bool IsInside(this in Vector2 v, in RectangleF rect)
        {
            var min = rect.Position;
            var max = rect.FarPosition;
            return v.X > min.X && v.Y > min.Y
                && v.X < max.X && v.Y < max.Y;
        }

        public static Point ToPoint(this in Vector2 v)
            => new Point((int)v.X, (int)v.Y);

        public static Point Floor(this in Vector2 v)
            => new Point((int)Math.Floor(v.X), (int)Math.Floor(v.Y));

        public static Point Round(this in Vector2 v)
            => new Point((int)Math.Round(v.X), (int)Math.Round(v.Y));

        public static Point Ceiling(this in Vector2 v)
            => new Point((int)Math.Ceiling(v.X), (int)Math.Ceiling(v.Y));


        public static bool IsValid(this in Vector2 v)
            => !float.IsNaN(v.X) && !float.IsInfinity(v.X)
            && !float.IsNaN(v.Y) && !float.IsInfinity(v.Y);

    }

}
