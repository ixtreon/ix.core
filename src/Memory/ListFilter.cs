﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ix.Core.Memory
{
    public class RefListFilter<TElement> : ListFilter<RefList<TElement>.Enumerator, TElement>
    {
        public RefListFilter(RefList<TElement> source, Func<TElement, bool> filter) 
            : base(source.GetEnumerator, filter)
        { }
    }

    public class DictionaryValuesFilter<TKey, TValue> 
        : ListFilter<Dictionary<TKey, TValue>.ValueCollection.Enumerator, TValue>
    {
        public DictionaryValuesFilter(Dictionary<TKey, TValue>.ValueCollection source, Func<TValue, bool> filter)
            : base(source.GetEnumerator, filter)
        { }
    }

    public class DictionaryKeysFilter<TKey, TValue>
        : ListFilter<Dictionary<TKey, TValue>.KeyCollection.Enumerator, TKey>
    {
        public DictionaryKeysFilter(Dictionary<TKey, TValue>.KeyCollection source, Func<TKey, bool> filter)
            : base(source.GetEnumerator, filter)
        { }
    }

    public class ListFilter<TEnumerator, TElement> : IEnumerable<TElement>
        where TEnumerator : IEnumerator<TElement>
    {
        readonly Func<TElement, bool> filter;
        readonly Func<TEnumerator> source;

        public ListFilter(Func<TEnumerator> source, Func<TElement, bool> filter)
        {
            this.filter = filter;
            this.source = source;
        }


        public IEnumerator<TElement> GetEnumerator()
            => new Enumerator(filter, source());

        IEnumerator IEnumerable.GetEnumerator()
            => new Enumerator(filter, source());

        public readonly struct Enumerator : IEnumerator<TElement>
        {
            readonly Func<TElement, bool> filter;
            readonly TEnumerator @base;

            public Enumerator(Func<TElement, bool> filter, TEnumerator @base)
            {
                this.filter = filter;
                this.@base = @base;
            }

            public TElement Current => @base.Current;
            object IEnumerator.Current => Current;
            public void Dispose() => @base.Dispose();
            public void Reset() => @base.Reset();
            public bool MoveNext()
            {
                while (@base.MoveNext())
                    if (filter(Current))
                        return true;
                return false;
            }
        }
    }
}
