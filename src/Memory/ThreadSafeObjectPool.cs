﻿using System;
using System.Collections.Concurrent;

namespace Ix.Memory
{
    /// <summary>
    /// A pool for reusing objects. Returned objects are retained and re-used. 
    /// Does not have facility to shrink the amount of free items, so retains max-size once it reaches it. 
    /// <para>
    /// This class is thread-safe. 
    /// </para>
    /// </summary>
    public class ThreadSafeObjectPool<T>
        where T : class, new()
    {
        static readonly Func<T> defaultConstructor = () => new T();

        readonly ConcurrentQueue<T> free;
        readonly Func<T> instanceConstructor;

        public ThreadSafeObjectPool()
        {
            free = new ConcurrentQueue<T>();
            instanceConstructor = defaultConstructor;
        }

        public ThreadSafeObjectPool(Func<T> instanceConstructor)
        {
            free = new ConcurrentQueue<T>();
            this.instanceConstructor = instanceConstructor;
        }

        public T Get()
        {
            if (free.TryDequeue(out var freed))
                return freed;

            return instanceConstructor();
        }

        public void Return(T value)
        {
            free.Enqueue(value);
        }

        public void ReclaimAll()
        {
            free.Clear();
        }
    }
}
