﻿using System;
using System.Collections.Generic;

namespace Ix.Core.Memory
{
    public static class CollectionExtensions
    {
        public static ListView<TIn, TOut> SelectByIndex<TIn, TOut>(this IReadOnlyList<TIn> source, Func<int, TOut> selector) 
            => new ListView<TIn, TOut>(source, selector);

        public static RefListFilter<TElement> FastWhere<TElement>(this RefList<TElement> source, Func<TElement, bool> filter)
            => new RefListFilter<TElement>(source, filter);

        public static DictionaryValuesFilter<TKey, TValue> FastWhere<TKey, TValue>(this Dictionary<TKey, TValue>.ValueCollection source, Func<TValue, bool> filter)
            => new DictionaryValuesFilter<TKey, TValue>(source, filter);

        public static DictionaryKeysFilter<TKey, TValue> FastWhere<TKey, TValue>(this Dictionary<TKey, TValue>.KeyCollection source, Func<TKey, bool> filter)
            => new DictionaryKeysFilter<TKey, TValue>(source, filter);
    }
}
