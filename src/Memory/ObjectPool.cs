﻿using System;
using System.Collections.Generic;

namespace Ix.Memory
{
    /// <summary>
    /// A pool for reusing objects. Returned objects are retained and re-used. 
    /// Does not have facility to shrink the amount of free items. 
    /// <para>
    /// NOTE: this class is NOT thread-safe.
    /// </para>
    /// </summary>
    public class ObjectPool<T>
        where T : class, new()
    {
        static readonly Func<T> defaultConstructor = () => new T();

        readonly Queue<T> free;
        readonly Func<T> instanceConstructor;

        public ObjectPool(int initialCapacity)
        {
            free = new Queue<T>(initialCapacity);
            instanceConstructor = defaultConstructor;
        }

        public ObjectPool(int initialCapacity, Func<T> instanceConstructor)
        {
            free = new Queue<T>(initialCapacity);
            this.instanceConstructor = instanceConstructor;
        }

        public T Get()
        {
            if (free.TryDequeue(out var freed))
                return freed;

            return instanceConstructor();
        }

        public void Return(T value)
        {
            free.Enqueue(value);
        }
    }
}
