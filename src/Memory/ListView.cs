using Ix.Core.Collections;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Ix.Core.Memory
{
    /// <summary>
    /// Static and extension methods for the <see cref="ListView{TIn, TOut}"/> class.
    /// </summary>
    public static class ListView
    {
        public static ListView<TElement, TOut> CreateView<TElement, TOut>(this IReadOnlyList<TElement> source, Func<int, TOut> selector)
            => new ListView<TElement, TOut>(source, selector);
    }


    /// <summary>
    /// A transform over each element of a list. 
    /// </summary>
    public class ListView<TIn, TOut> : IIndexable<TOut>, IReadOnlyList<TOut>
    {
        readonly Func<int, TOut> mapping;
        readonly IReadOnlyList<TIn> source;

        public ListView(IReadOnlyList<TIn> source, Func<int, TOut> mapping)
        {
            this.mapping = mapping;
            this.source = source;
        }

        // IIndexable implementation
        public int Count => source.Count;
        public TOut this[int index] => mapping(index);
    }
}
