﻿using System;
using System.Collections.Generic;

namespace Ix.Memory
{
    public struct Allocator
    {
        public static Allocator Create() => new Allocator(new List<int>());

        readonly List<int> freeIDs;
        int maxId;

        Allocator(List<int> freeIDs)
        {
            this.freeIDs = freeIDs;
            maxId = default;
        }

        public void Reset()
        {
            VerifyInitialized();

            freeIDs.Clear();
            maxId = 0;
        }

        public int New()
        {
            VerifyInitialized();

            if (freeIDs.Count > 0)
                return freeIDs.Pop();
            return maxId++;
        }

        public void Release(int i)
        {
            VerifyInitialized();

            freeIDs.Add(i);
        }

        public void Clear()
        {
            maxId = 0;
            freeIDs.Clear();
        }

        void VerifyInitialized()
        {
            if(freeIDs == null) throw new InvalidOperationException("Attempted to call a method on an uninitialized instance. ");
        }

        public int Count => maxId - freeIDs.Count;
    }
}
