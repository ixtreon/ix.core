﻿using System;

namespace Ix.Memory
{
    public static class PoolMarshal
    {
        internal static void Clone<T>(DynamicPool<T> source, DynamicPool<T> destination)
            where T : struct
        {
            source.CloneInto(destination);
        }
    }

    /// <summary>
    /// An array-backed pool of objects. 
    /// You can get a fresh index, or release one already in use. 
    /// <para>
    /// NOTE: The underlying storage may move - so don't hold to any refs!
    /// </para>
    /// </summary>
    public class DynamicPool<T>
        where T : struct
    {
        // a stack of the released indices
        int freeCount;
        int[] free;

        // underlying array of values
        // releasing creates holes whose indices go into the `free` stack
        int nodeCount;
        public T[] nodes;

        public DynamicPool(int capacity)
        {
            free = new int[capacity];
            freeCount = nodeCount = 0;

            nodes = new T[capacity];
        }

        internal void CloneInto(DynamicPool<T> destination)
        {
            destination.freeCount = freeCount;
            destination.nodeCount = nodeCount;

            if (destination.free.Length < free.Length)
                destination.free = new int[free.Length];
            if (destination.nodes.Length < nodes.Length)
                destination.nodes = new T[nodes.Length];

            Array.Copy(free, destination.free, freeCount);          // use `freeCount` as consecutive entries
            Array.Copy(nodes, destination.nodes, nodes.Length);     // 

            throw new NotImplementedException();
        }
        /// <summary>
        /// Gets a reference to the item at the requested position.
        /// <para>
        /// NOTE: Do not hold on to such references, as these get invalidated when the internal array changes size.
        /// </para>
        /// </summary>
        public ref T this[int i]
            => ref nodes[i];

        /// <summary>
        /// Gets the number of indices in use.
        /// </summary>
        public int Count
            => nodeCount - freeCount;

        /// <summary>
        /// Returns a fresh object ID.
        /// </summary>
        public int New()
        {
            if (freeCount > 0)
                return free[--freeCount];

            var id = nodeCount++;
            if (id == nodes.Length)
                Array.Resize(ref nodes, nodes.Length * 2);

            return id;
        }

        public void Clear()
        {
            nodeCount = 0;
            freeCount = 0;
            Array.Clear(nodes, 0, nodes.Length);
        }

        /// <summary>
        /// Releases the given index and makes it available for reuse.
        /// <para>
        /// NOTE: Depending on your use case you may want to clear (set to <c>default</c>)
        /// the underlying storage manually before calling this method.
        /// </para>
        /// </summary>
        public void Release(int id)
        {
            if (freeCount == free.Length)
                Array.Resize(ref free, freeCount * 2);
            free[freeCount++] = id;
        }
    }
}
