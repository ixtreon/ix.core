﻿using Ix.Core.Collections;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Ix.Core.Memory
{
    /// <summary>
    /// Don't shoot yourself. This may (will) resize, and your persistent refs will cause a memory leak, or worse. 
    /// <para/>
    /// An array-backed custom implementation of <see cref="List{T}"/> which returns references to the items inside.
    /// </summary>
    public class RefList<T> : IList<T>, IReadOnlyList<T>, IIndexable<T>, IRefIndexable<T>
    {
        int count;
        T[] store;

        public RefList(int initialCapacity = 8)
        {
            if (initialCapacity <= 0)
                throw new ArgumentOutOfRangeException(nameof(initialCapacity), "The initial capacity must be positive");

            store = new T[initialCapacity];
        }

        public RefList(ReadOnlySpan<T> source)
        {
            count = source.Length;
            store = new T[source.Length];

            source.CopyTo(store);
        }

        public ref T Last() => ref store[count - 1];

        ref T getOrThrow(int i)
        {
            if (i < 0 || i >= count) throw new IndexOutOfRangeException();
            return ref store[i];
        }

        T IList<T>.this[int index]
        {
            get => getOrThrow(index);
            set => getOrThrow(index) = value;
        }
        T IReadOnlyList<T>.this[int index] => getOrThrow(index);
        T IIndexable<T>.this[int index] => getOrThrow(index);
        bool ICollection<T>.Contains(T item) => Contains(item);
        void ICollection<T>.Add(T item) => Add(item);
        int IList<T>.IndexOf(T item) => IndexOf(item);
        bool ICollection<T>.IsReadOnly => false;

        public ref T this[int index] => ref getOrThrow(index);

        public int Count => count;

        public bool IsEmpty => count == 0;

        public void Add(in T item)
        {
            if (count == store.Length)
                Array.Resize(ref store, count * 2);

            store[count++] = item;
        }

        public void Clear()
        {
            Array.Clear(store, 0, count);
            count = 0;
        }

        public void EnsureCapacity(int requiredCapacity)
        {
            if (store.Length >= requiredCapacity)
                return;

            var capacity = store.Length;
            while (capacity < requiredCapacity)
                capacity *= 2;

            Array.Resize(ref store, capacity);
        }

        public bool Contains(in T item)
            => IndexOf(item) >= 0;

        public void CopyTo(T[] array, int arrayIndex)
            => Array.Copy(store, 0, array, arrayIndex, count);

        public int IndexOf(in T item)
        {
            for (int i = 0; i < count; i++)
                if (Equals(store[i], item))
                    return i;
            return -1;
        }

        public Span<T> AsSpan() => store.AsSpan(0, count);
        public Span<T> AsSpan(int start) => store.AsSpan(start, count - start);

        public void InsertRange(int start, T[] values)
            => InsertRange(start, values.AsSpan());

        public void InsertRange(int start, ReadOnlySpan<T> values)
        {
            if (start < 0 || start > count)
                throw new IndexOutOfRangeException();

            var nToMoveOver = count - start;
            count += values.Length;
            EnsureCapacity(count);

            Array.Copy(store, start, store, start + values.Length, nToMoveOver);    // move items after the inserted range
            values.CopyTo(store.AsSpan(start));

        }

        public void InsertRange(int start, int length, T value)
        {
            if (start < 0 || start > count)
                throw new IndexOutOfRangeException();

            var end = start + length;
            var nToMoveOver = count - start;
            count += length;
            EnsureCapacity(count);

            Array.Copy(store, start, store, end, nToMoveOver);
            while (start < end)
                store[start++] = value;
        }

        public void MoveElement(int currentPosition, int newPosition)
        {
            if (currentPosition < 0 || currentPosition >= count)
                throw new IndexOutOfRangeException();

            if (newPosition < 0 || newPosition >= count)
                throw new IndexOutOfRangeException();

            if (newPosition == currentPosition)
                return;

            var curV = store[currentPosition];
            if (newPosition > currentPosition)
            {
                // ..., e0 [cp], e1, e2, ..., en-1, en [np], ....
                var lenToCopy = newPosition - currentPosition;
                Array.Copy(store, currentPosition + 1, store, currentPosition, lenToCopy);
            }
            else
            {
                // ..., e0 [np], e1, e2, ..., en-1, en [cp], ....
                var lenToCopy = currentPosition - newPosition;
                Array.Copy(store, newPosition, store, newPosition + 1, lenToCopy);
            }
            store[newPosition] = curV;
        }

        public void Insert(int index, T item) => InsertRange(index, 1, item);
        public void AddRange(int count, T value) => InsertRange(this.count, count, value);
        public void AddRange(ReadOnlySpan<T> values) => InsertRange(count, values);
        public void AddEmptyRange(int count)
        {
            this.count += count;
            EnsureCapacity(this.count);
        }

        public void AddDefault(int count)
        {
            EnsureCapacity(this.count + count);
            Array.Clear(store, this.count, count);
            this.count += count;
        }

        /// <summary>
        /// Note: O(n) due to this being a list.
        /// </summary>
        public bool Remove(T item)
        {
            var index = IndexOf(item);
            if (index < 0)
                return false;

            RemoveAt(index);
            return true;
        }

        public void RemoveAt(int index) => RemoveRange(index, 1);
        public void RemoveLast() => RemoveRange(count - 1, 1);

        public void RemoveRange(int start, int length)
        {
            var end = start + length;
            if (start < 0 || end > count)
                throw new IndexOutOfRangeException("out of range etc.");

            Array.Copy(store, end, store, start, count - end);
            count -= length;
        }

        public int RemoveWhereIndexMatches(Func<int, bool> predicate)
        {
            int readFrom, writeTo;
            for (readFrom = 0, writeTo = 0; readFrom < count; readFrom++)
                if (!predicate(readFrom))
                    store[writeTo++] = store[readFrom];

            count = writeTo;
            return readFrom - writeTo;
        }


        public Enumerator GetEnumerator() => new Enumerator(this);
        IEnumerator<T> IEnumerable<T>.GetEnumerator() => new Enumerator(this);
        IEnumerator IEnumerable.GetEnumerator() => new Enumerator(this);

        public struct Enumerator : IEnumerator<T>
        {
            readonly RefList<T> @base;
            int index;

            public Enumerator(RefList<T> @base) : this()
            {
                this.@base = @base;
            }

            public T Current => @base.store[index - 1];
            object IEnumerator.Current => Current;
            public void Dispose() { }
            public void Reset() => index = 0;
            public bool MoveNext()
            {
                if (index < @base.Count)
                {
                    index++;
                    return true;
                }

                index = @base.Count + 1;
                return false;
            }
        }
    }
}
