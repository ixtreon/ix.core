﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Shanism.Common
{
    /// <summary>
    /// Provides fast, generic access to the values of an enumeration. 
    /// </summary>
    /// <typeparam name="TEnum">The enumeration to access the values of. </typeparam>
    public static class Enum<TEnum>
        where TEnum : Enum
    {
        static class Convert
        {
            public static Func<TEnum, int> ToInt { get; } = ToPrimitive<int>();
            public static Func<int, TEnum> FromInt { get; } = ToEnum<int>();

            public static Func<TEnum, byte> ToByte { get; } = ToPrimitive<byte>();
            public static Func<byte, TEnum> FromByte { get; } = ToEnum<byte>();


            static Func<TEnum, TPrimitive> ToPrimitive<TPrimitive>()
            {
                var p = Expression.Parameter(typeof(TEnum));
                var c = Expression.ConvertChecked(p, typeof(TPrimitive));
                return Expression.Lambda<Func<TEnum, TPrimitive>>(c, p).Compile();
            }
            static Func<TPrimitive, TEnum> ToEnum<TPrimitive>()
            {
                var p = Expression.Parameter(typeof(TPrimitive));
                var c = Expression.ConvertChecked(p, typeof(TEnum));
                return Expression.Lambda<Func<TPrimitive, TEnum>>(c, p).Compile();
            }
        }

        public static TypeCode BaseType = Type.GetTypeCode(typeof(TEnum).GetEnumUnderlyingType());


        static readonly TEnum[] _values = Enum.GetValues(typeof(TEnum))
            .Cast<TEnum>()
            .OrderBy(v => v)
            .ToArray();

        static readonly string[] _names = _values
            .Select(v => Enum.GetName(typeof(TEnum), v))
            .ToArray();


        /// <summary>
        /// Gets the number of constants defined in this enum type. 
        /// </summary>
        public static int Count { get; } = Values.Length;

        public static TEnum MaxValue { get; } = _values.Max();
        public static TEnum MinValue { get; } = _values.Min();


        /// <summary>
        /// Gets all declared values of this enum type,
        /// ordered by their numerical representation.
        /// </summary>
        public static ReadOnlySpan<TEnum> Values => _values.AsSpan();

        /// <summary>
        /// Gets the names of all values in this enum type. 
        /// </summary>
        public static ReadOnlySpan<string> Names => _names.AsSpan();

        public static TEnum FromInt(int e) => Convert.FromInt(e);
        public static int ToInt(TEnum e) => Convert.ToInt(e);

        public static TEnum FromByte(byte e) => Convert.FromByte(e);
        public static byte ToByte(TEnum e) => Convert.ToByte(e);


    }

    public static class EnumExt
    {
        /// <summary>
        /// Gets the next value in the specified enum. 
        /// Order is defined by the numerical representation of the values. 
        /// </summary>
        /// <typeparam name="TEnum">The type of the enum.</typeparam>
        /// <param name="currentValue">The element to get the next value for.</param>
        /// <param name="nextValue">The next value for the given element, ot default if the input was unrecognized.</param>
        public static bool TryFindNext<TEnum>(this TEnum currentValue, out TEnum nextValue)
            where TEnum : Enum
        {
            var vals = Enum<TEnum>.Values;
            for(var i = 0; i < vals.Length - 1; i++)
                if(currentValue.Equals(vals[i]))
                {
                    nextValue = vals[i + 1];
                    return true;
                }

            if(currentValue.Equals(vals[^1]))
            {
                nextValue = vals[0];
                return true;
            }

            nextValue = default;
            return false;
        }

        /// <summary>
        /// Gets the next value in the specified enum. 
        /// Order is defined by the numerical representation of the values. 
        /// </summary>
        /// <typeparam name="TEnum">The type of the enum.</typeparam>
        /// <param name="currentValue">The element to get the next value for.</param>
        public static TEnum FindNext<TEnum>(this TEnum currentValue)
            where TEnum : Enum
        {
            if (!TryFindNext(currentValue, out var nextValue))
                throw new ArgumentOutOfRangeException(nameof(currentValue), currentValue, "Unknown enum value");
            return nextValue;
        }
    }
}
