﻿using Ix.Core.Memory;
using Shanism.Common.ZFormat;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

namespace Shanism.Common.Util
{
    /// <summary>
    /// Records perf data.
    /// </summary>
    public class PerfCounter
    {
        readonly ConcurrentDictionary<string, long> stats = new();
        long totalTicks;

        public string Name { get; }

        public PerfCounter(string name)
        {
            Name = name;
        }


        public long this[string category]
            => stats[category];

        public PerformanceTiming Time(string categoryName)
        {
            return new PerformanceTiming(Stopwatch.GetTimestamp(), categoryName, this);
        }

        void EndTiming(string category, long duration)
        {
            static long Add(string category, long duration) => duration;
            static long Update(string category, long current, long duration) => current + duration;

            stats.AddOrUpdate(category, Add, Update, duration);
            Interlocked.Add(ref totalTicks, duration);
        }

        public readonly struct PerformanceTiming : IDisposable
        {
            readonly long startAt;
            readonly string name;
            readonly PerfCounter counter;

            public PerformanceTiming(long startAt, string name, PerfCounter counter)
            {
                this.startAt = startAt;
                this.name = name;
                this.counter = counter;
            }

            public void Dispose()
                => counter.EndTiming(name, Stopwatch.GetTimestamp() - startAt);
        }

        /// <summary>
        /// Resets the timings for all performance categories in this instance.. 
        /// </summary>
        public void Reset()
        {
            stats.Clear();
            totalTicks = 0;
        }


        public void PrintPerformanceData(RefList<char> destination, float realTimeMs)
        {
            var realTicks = realTimeMs * Stopwatch.Frequency / 1000;

            var timeSlice = totalTicks / realTicks * 100;
            destination.AppendFormat("# {0}: {1:00.00}%", Name, timeSlice);
            destination.AppendLine();

            foreach (var kvp in stats)
            {
                var name = kvp.Key;
                var percRealTime = kvp.Value * 100 / realTicks;
                var percThisInstance = totalTicks == 0 ? 0 : kvp.Value * 100 / totalTicks;

                WriteProgressBar(destination, (int)percThisInstance, barLength: 20);
                destination.AppendFormat(" {0} {1:00.0}%", name, percRealTime);
                destination.AppendLine();
            }
        }

        void WriteProgressBar(RefList<char> sb, int perc, int barLength)
        {
            if (totalTicks > 0)
            {
                var nPluses = perc * barLength / 100;

                sb.Append(" [");
                sb.AddRange(nPluses, '!');
                sb.AddRange(barLength - nPluses, '.');
                sb.Append("] ");
            }
        }
    }
}
