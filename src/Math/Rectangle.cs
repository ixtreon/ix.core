﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Numerics;

namespace Ix.Math
{
    /// <summary>
    /// Represents a rectangle in the 2D plane. 
    /// </summary>
    public readonly struct Rectangle : IEquatable<Rectangle>
    {
        /// <summary>
        /// An empty rectangle positioned at the origin. 
        /// </summary>
        public static readonly Rectangle Empty = new Rectangle();


        /// <summary>
        /// Gets or sets the position of the bottom-left (low) corner of the rectangle. 
        /// </summary>
        public Point Position { get; }

        /// <summary>
        /// Gets or sets the size of the rectangle. 
        /// </summary>
        public Point Size { get; }


        /// <summary>
        /// Initializes a new instance of the <see cref="Rectangle"/> struct. 
        /// </summary>
        /// <param name="position">The position of the rectangle.</param>
        /// <param name="size">The size of the rectangle.</param>
        public Rectangle(Point position, Point size)
        {
            Debug.Assert(size.X >= 0 && size.Y >= 0);

            Position = position;
            Size = size;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Rectangle"/> struct.
        /// </summary>
        /// <param name="x">The x coordinate of the rectangle.</param>
        /// <param name="y">The y coordinate of the rectangle.</param>
        /// <param name="width">The width of the rectangle.</param>
        /// <param name="height">The height of the rectangle.</param>
        public Rectangle(int x, int y, int width, int height)
        {
            Debug.Assert(width >= 0 && height >= 0);

            Position = new Point(x, y);
            Size = new Point(width, height);
        }


        public static Rectangle Normalise(Point position, Point size)
        {
            var (x, y) = position;
            var (w, h) = size;

            if (w < 0)
            {
                x += w;
                w = -w;
            }

            if (h < 0)
            {
                y += h;
                h = -h;
            }

            return new Rectangle(x, y, w, h);
        }

        /// <summary>
        /// Gets the far position (high X, high Y) of the rectangle. 
        /// </summary>
        /// <value>
        /// The far position.
        /// </value>
        public Point FarPosition => Position + Size;

        /// <summary>
        /// Gets the vector that lies at the center of this rectangle. 
        /// </summary>
        public Vector2 Center => Position + (Vector2)Size / 2f;

        /// <summary>
        /// Gets the low X edge of the rectangle. 
        /// </summary>
        public int X => Position.X;

        /// <summary>
        /// Gets the low Y edge of the rectangle. 
        /// </summary>
        public int Y => Position.Y;

        /// <summary>
        /// Gets the width of the rectangle. 
        /// </summary>
        public int Width => Size.X;

        /// <summary>
        /// Gets the height of the rectangle. 
        /// </summary>
        public int Height => Size.Y;

        /// <summary>
        /// Gets the left (low X) edge of the rectangle. 
        /// </summary>
        public int Left => X;

        /// <summary>
        /// Gets the right (high X) edge of the rectangle. 
        /// </summary>
        public int Right => X + Width;

        /// <summary>
        /// Gets the bottom (low Y) edge of the rectangle. 
        /// </summary>
        public int Bottom => Y;

        /// <summary>
        /// Gets the top (high Y) edge of the rectangle. 
        /// </summary>
        public int Top => Y + Height;


        /// <summary>
        /// Gets the area of the rectangle. 
        /// </summary>
        public int Area => Width * Height;


        public static implicit operator RectangleF(Rectangle r) 
            => new RectangleF(r.Position, r.Size);

        /// <summary>
        /// Offsets the rectangle by the specified amount. 
        /// </summary>
        public static Rectangle operator +(Rectangle r, Point p) 
            => new Rectangle(r.Position + p, r.Size);

        /// <summary>
        /// Offsets the rectangle by the specified amount. 
        /// </summary>
        public static Rectangle operator -(Rectangle r, Point p) 
            => new Rectangle(r.Position - p, r.Size);


        public static RectangleF operator *(Rectangle r, Vector2 times) 
            => new RectangleF(r.Position * times, r.Size * times);

        public Rectangle Inflate(int bottom, int right, int top, int left)
            => Inflate(new Point(left, bottom), new Point(right, top));

        public Rectangle Inflate(Point botLeft, Point topRight)
            => new Rectangle(Position - botLeft, Size + botLeft + topRight);

        /// <summary>
        /// Iterates all points within this rectangle. First X then Y. 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Point> Iterate()
        {
            for (int ix = 0; ix < Size.X; ix++)
                for (int iy = 0; iy < Size.Y; iy++)
                    yield return new Point(X + ix, Y + iy);
        }

        /// <summary>
        /// Gets the intersection (common area) of the two rectangles. 
        /// </summary>
        /// <param name="rectangle"></param>
        /// <returns></returns>
        public Rectangle Intersect(in Rectangle rectangle)
        {
            var x = System.Math.Max(rectangle.X, X);
            var y = System.Math.Max(rectangle.Y, Y);
            var farX = System.Math.Min(rectangle.Right, Right);
            var farY = System.Math.Min(rectangle.Top, Top);

            return new Rectangle(x, y, farX - x, farY - y);
        }


        /// <summary>
        /// Returns whether the given point lies inside this rectangle. 
        /// </summary>
        public bool Contains(in Point p)
            => p.X >= Left && p.X < Right
            && p.Y >= Bottom && p.Y < Top;

        /// <summary>
        /// Returns whether the given vector lies inside this rectangle. 
        /// </summary>
        public bool Contains(in Vector2 v)
            => v.X >= Left && v.X < Right
            && v.Y >= Bottom && v.Y < Top;

        public void Deconstruct(out Point position, out Point size)
        {
            (position, size) = (Position, Size);
        }

        public override string ToString() => $"[{X}, {Y}, {Width}, {Height}]";
        public override int GetHashCode() => (Position, Size).GetHashCode();
        public bool Equals(Rectangle other) => Position == other.Position && Size == other.Size;
        public override bool Equals(object obj) => (obj is Rectangle other) && Equals(other);
        public static bool operator ==(Rectangle a, Rectangle b) => a.Equals(b);
        public static bool operator !=(Rectangle a, Rectangle b) => !a.Equals(b);


        public AreaEnumerable EnumerateArea()
            => new AreaEnumerable(Position, Position + Size);
    }
}
