﻿using System.Numerics;

namespace Ix.Math
{
    public struct Padding
    {
        Vector2 topLeft;
        Vector2 botRight;

        public Padding(int val)
        {
            topLeft = new Vector2(val);
            botRight = new Vector2(val);
        }

        public Padding(int h, int v)
        {
            topLeft = new Vector2(h, v);
            botRight = new Vector2(h, v);
        }

        public Padding(float top, float right, float bottom, float left)
        {
            topLeft = new Vector2(top, left);
            botRight = new Vector2(bottom, right);
        }

        /// <summary>
        /// Gets the horizontal (X) distance.
        /// </summary>
        public float Horizontal => Left + Right;
        /// <summary>
        /// Gets the vertical (Y) distance.
        /// </summary>
        public float Vertical => Top + Bottom;

        /// <summary>
        /// Gets the total distance in each direction.
        /// </summary>
        public Vector2 Total => topLeft + botRight;

        /// <summary>
        /// Gets or sets the top distance.
        /// </summary>
        public float Top
        {
            get => topLeft.X;
            set => topLeft.X = value;
        }

        /// <summary>
        /// Gets or sets the bottom distance.
        /// </summary>
        public float Bottom
        {
            get => botRight.X;
            set => botRight.X = value;
        }

        /// <summary>
        /// Gets or sets the left distance.
        /// </summary>
        public float Left
        {
            get => topLeft.Y;
            set => topLeft.Y = value;
        }

        /// <summary>
        /// Gets or sets the right distance.
        /// </summary>
        public float Right
        {
            get => botRight.Y;
            set => botRight.Y = value;
        }
    }
}
