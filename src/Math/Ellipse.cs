﻿using System.Numerics;

namespace Ix.Math
{
    /// <summary>
    /// A 2D axis-aligned ellipse.
    /// </summary>
    public readonly struct Ellipse
    {
        /// <summary>
        /// Gets the center of the ellipse.
        /// </summary>
        public Vector2 Center { get; }

        /// <summary>
        /// Gets the radius of the ellipse.
        /// </summary>
        public Vector2 Radius { get; }

        public Ellipse(Vector2 center, float radius)
        {
            Center = center;
            Radius = new Vector2(radius);
        }

        public Ellipse(Vector2 center, Vector2 radius)
        {
            Center = center;
            Radius = radius;
        }

        public Ellipse(in RectangleF boundingBox)
        {
            Center = boundingBox.Center;
            Radius = boundingBox.Size / 2;
        }

        public bool Contains(Vector2 v) 
            => ((v - Center) / Radius).LengthSquared() < 1;


        public bool Intersects(in RectangleF rect)
            => ((Center.Clamp(rect) - Center) / Radius).LengthSquared() < 1;

        public RectangleF GetBoundingBox()
            => new RectangleF(Center - Radius, Radius * 2);
    }
}
