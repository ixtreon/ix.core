﻿using System;
using System.Collections.Generic;
using System.Numerics;

namespace Ix.Math
{
    /// <summary>
    /// A rectangle in the 2D plane. 
    /// </summary>
    public readonly struct RectangleF : IEquatable<RectangleF>
    {
        /// <summary>
        /// Gets the empty rectangle that lies at the origin. 
        /// </summary>
        public static RectangleF Empty { get; } = new RectangleF();


        /// <summary>
        /// Gets the position of the bottom-left (low) corner of the transformed area. 
        /// </summary>
        public readonly Vector2 Position;

        /// <summary>
        /// Gets the size of the rectangle. 
        /// </summary>
        public readonly Vector2 Size;


        #region Property Shortcuts

        /// <summary>
        /// Gets the point at the center of this rectangle.
        /// </summary>
        public Vector2 Center => Position + Size / 2;
        /// <summary>
        /// Gets the top right (high X, high Y) corner of this rectangle. 
        /// </summary>
        public Vector2 FarPosition => Position + Size;

        /// <summary>
        /// Gets the low X coordinate of this rectangle. 
        /// </summary>
        public float X => Position.X;
        /// <summary>
        /// Gets the low Y coordinate of this rectangle. 
        /// </summary>
        public float Y => Position.Y;

        /// <summary>
        /// Gets the width of the rectangle. 
        /// </summary>
        public float Width => Size.X;
        /// <summary>
        /// Gets the height of the rectangle. 
        /// </summary>
        public float Height => Size.Y;

        /// <summary>
        /// Gets the left (low X) edge of the rectangle. 
        /// </summary>
        public float Left => X;
        /// <summary>
        /// Gets the right (high X) edge of the rectangle. 
        /// </summary>
        public float Right => X + Width;
        /// <summary>
        /// Gets the top (low Y) edge of the rectangle. 
        /// </summary>
        public float Top => Y;
        /// <summary>
        /// Gets the bottom (high Y) edge of the rectangle. 
        /// </summary>
        public float Bottom => Y + Height;

        /// <summary>
        /// Gets the bottom left (low X, low Y) corner of this rectangle. 
        /// </summary>
        public Vector2 TopLeft => new Vector2(Left, Top);
        /// <summary>
        /// Gets the bottom right (high X, low Y) corner of this rectangle. 
        /// </summary>
        public Vector2 TopRight => new Vector2(Right, Top);
        /// <summary>
        /// Gets the top left (low X, high Y) corner of this rectangle. 
        /// </summary>
        public Vector2 BottomLeft => new Vector2(Left, Bottom);
        /// <summary>
        /// Gets the top right (high X, high Y) corner of this rectangle. 
        /// </summary>
        public Vector2 BottomRight => new Vector2(Right, Bottom);

        /// <summary>
        /// Gets the area of this rectangle.
        /// </summary>
        public float Area => Width * Height;
        /// <summary>
        /// Gets the perimeter of this rectangle.
        /// </summary>
        public float Perimiter => (Width + Height) * 2;

        #endregion


        #region Binary Operators

        /// <summary>
        /// Multiplies both the position and the size of the rectangle by the given vector.
        /// </summary>
        public static RectangleF operator *(RectangleF r, Vector2 p) => new RectangleF(r.Position * p, r.Size * p);
        /// <summary>
        /// Divides both the position and the size of the rectangle by the given vector.
        /// </summary>
        public static RectangleF operator /(RectangleF r, Vector2 p) => new RectangleF(r.Position / p, r.Size / p);
        /// <summary>
        /// Adds the given vector to the rectangle's position.
        /// </summary>
        public static RectangleF operator +(RectangleF r, Vector2 p) => new RectangleF(r.Position + p, r.Size);
        /// <summary>
        /// Subtracts the given vector from the rectangle's position.
        /// </summary>
        public static RectangleF operator -(RectangleF r, Vector2 p) => new RectangleF(r.Position - p, r.Size);

        /// <summary>
        /// Multiplies both the position and the size of the rectangle by the given scalar.
        /// </summary>
        public static RectangleF operator *(RectangleF r, float f) => new RectangleF(r.Position * f, r.Size * f);
        /// <summary>
        /// Divides both the position and the size of the rectangle by the given scalar.
        /// </summary>
        public static RectangleF operator /(RectangleF r, float f) => new RectangleF(r.Position / f, r.Size / f);
        /// <summary>
        /// Adds the given scalar to the rectangle's position.
        /// </summary>
        public static RectangleF operator +(RectangleF r, float f) => r + new Vector2(f);
        /// <summary>
        /// Subtracts the given scalar from the rectangle's position.
        /// </summary>
        public static RectangleF operator -(RectangleF r, float f) => r - new Vector2(f);

        public static bool operator ==(RectangleF a, RectangleF b) => a.Equals(b);
        public static bool operator !=(RectangleF a, RectangleF b) => !a.Equals(b);

        #endregion


        /// <summary>
        /// Initializes a new instance of the <see cref="RectangleF"/> struct.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        public RectangleF(float x, float y, float width, float height)
        {
            System.Diagnostics.Debug.Assert(width >= 0 && height >= 0);

            Position = new Vector2(x, y);
            Size = new Vector2(width, height);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RectangleF"/> struct
        /// at the specified position and size. 
        /// </summary>
        /// <param name="position">The position of the rectangle. .</param>
        /// <param name="size">The size of the rectangle. .</param>
        public RectangleF(Vector2 position, Vector2 size)
        {
            System.Diagnostics.Debug.Assert(size.X >= 0 && size.Y >= 0);

            Position = position;
            Size = size;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RectangleF"/> struct 
        /// that is a copy of the given rectangle. 
        /// </summary>
        /// <param name="src">The source rectangle.</param>
        public RectangleF(RectangleF src)
        {
            Position = src.Position;
            Size = src.Size;
        }


        public static RectangleF Normalise(Vector2 position, Vector2 size)
        {
            position = Vector2.Min(position, position + size);
            size = Vector2.Abs(size);
            return new RectangleF(position, size);
        }
        
        public void Deconstruct(out Vector2 position, out Vector2 size)
        {
            position = Position;
            size = Size;
        }

        /// <summary>
        /// Gets a point along the rectangle. Both coordinates range from 0 to 1. 
        /// </summary>
        public Vector2 GetPoint(float xDistance, float yDistance)
            => Position + Size * new Vector2(xDistance, yDistance);

        /// <summary>
        /// Casts this instance to a <see cref="Rectangle"/>. 
        /// </summary>
        public Rectangle ToRectangle() => new Rectangle(Position.ToPoint(), Size.ToPoint());
        public Rectangle Round() => new Rectangle(Position.Round(), Size.Round());
        public Rectangle Ceiling() => new Rectangle(Position.Ceiling(), Size.Ceiling());
        public Rectangle Floor() => new Rectangle(Position.Floor(), Size.Floor());


        /// <summary>
        /// Gets the intersection (common area) of the two rectangles. 
        /// </summary>
        public RectangleF Intersect(in RectangleF rectangle)
        {
            var near = Vector2.Max(rectangle.Position, Position);
            var far = Vector2.Min(rectangle.FarPosition, FarPosition);

            return new RectangleF(near, Vector2.Max(Vector2.Zero, far - near));
        }

        public bool Intersects(in RectangleF other) => Intersects(other.Position, other.Size);

        public bool Intersects(in Vector2 pos, in Vector2 sz)
        {
            var x = (pos + sz) - Position;  // >0 if our topLeft before their botRight
            var y = FarPosition - pos;      // >0 if their topLeft before our botRight

            return x.X > 0 && x.Y > 0 && y.X > 0 && y.Y > 0;
        }

        public bool Intersects(in Ellipse e)
            => ((e.Center.Clamp(this) - e.Center) / e.Radius).LengthSquared() < 1;

        /// <summary>
        /// Gets the union (bounding box) of the two rectangles.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public RectangleF Union(in RectangleF other)
        {
            var near = Vector2.Min(Position, other.Position);
            var high = Vector2.Max(FarPosition, other.FarPosition);

            return new RectangleF(near, high - near);
        }

        /// <summary>
        /// Inflates the rectangle in all direction by the specified amount. 
        /// </summary>
        public RectangleF Inflate(float amount) => Deflate(-amount);
        public RectangleF Inflate(Vector2 topLeft, Vector2 bottomRight) => Deflate(-topLeft, -bottomRight);
        public RectangleF Inflate(float top = 0, float left = 0, float bottom = 0, float right = 0) 
            => Deflate(-top, -left, -bottom, -right);

        /// <summary>
        /// Deflates the rectangle from all direction by the specified amount. 
        /// </summary>
        public RectangleF Deflate(float amount)
        {
            var am = Vector2.Min(new Vector2(amount * 2), Size);
            return new RectangleF(Position + am / 2, Size - am);
        }

        public RectangleF Deflate(float top, float left, float bottom, float right) 
            => Deflate(new Vector2(left, top), new Vector2(right, bottom));
        public RectangleF Deflate(in Vector2 topLeft, in Vector2 bottomRight)
        {
            // not quite right if resulting W/H is zero, but it's a 0-area rect
            var dOffset = Vector2.Min(Size, topLeft);
            var dSize = Vector2.Min(Size, topLeft + bottomRight);
            return new RectangleF(Position + topLeft, Size - dSize);
        }


        /// <summary>
        /// Returns whether this instance fully contains the other instance.
        /// </summary>
        public bool FullyContains(in RectangleF other)
        {
            var db = FarPosition - other.FarPosition;

            return other.Position.X >= Position.X && other.Position.Y >= Position.Y && db.X >= 0 && db.Y >= 0;
        }

        public override bool Equals(object obj) => (obj is RectangleF other) && this == other;
        public bool Equals(RectangleF other) => Position == other.Position && Size == other.Size;
        public override int GetHashCode() => (Position, Size).GetHashCode();

        public override string ToString() => ToString("N2");
        public string ToString(string format)
            => $"[{X.ToString(format)}, {Y.ToString(format)}, {Width.ToString(format)}, {Height.ToString(format)}]";

    }
}
