﻿using System.Diagnostics;
using System.Numerics;

namespace Ix.Math.Simd
{

    public delegate Vector<T> UnaryVectorTransform<T>(Vector<T> source) where T : struct;
    public delegate Vector<T> BinaryVectorTransform<T>(Vector<T> left, Vector<T> right) where T : struct;
    
    public static class SimdExtensions
    {

        static class Helpers<T> where T : struct
        {
            public static readonly UnaryVectorTransform<T> Negate = Vector.Negate<T>;
            public static readonly BinaryVectorTransform<T> Add = Vector.Add<T>;
            public static readonly BinaryVectorTransform<T> Subtract = Vector.Subtract<T>;
            public static readonly BinaryVectorTransform<T> Multiply = Vector.Multiply<T>;
            public static readonly BinaryVectorTransform<T> Divide = Vector.Divide<T>;
        }

        public static void Negate<T>(this T[] a) where T : struct
            => Transform(a, Helpers<T>.Negate);

        public static void Add<T>(this T[] a, T[] b) where T : struct
            => Transform(a, b, Helpers<T>.Add);

        public static void Subtract<T>(this T[] a, T[] b) where T : struct
            => Transform(a, b, Helpers<T>.Subtract);

        public static void Multiply<T>(this T[] a, T[] b) where T : struct
            => Transform(a, b, Helpers<T>.Multiply);
        public static void Divide<T>(this T[] a, T[] b) where T : struct
            => Transform(a, b, Helpers<T>.Divide);


        public static T[] Transform<T>(this T[] a, UnaryVectorTransform<T> transform)
            where T : struct
        {
            var max = (a.Length - 1) / Vector<T>.Count;
            for(int i = 0; i <= max; i++)
                set(a, i, transform(get(a, i)));
            return a;
        }

        public static T[] Transform<T>(this T[] a, T[] b, BinaryVectorTransform<T> transform)
            where T : struct
        {
            Debug.Assert(a.Length == b.Length);

            var max = (a.Length - 1) / Vector<T>.Count;
            for(int i = 0; i <= max; i++)
                set(a, i, transform(get(a, i), get(b, i)));
            return a;
        }

        static Vector<T> get<T>(in T[] source, int id) where T : struct
            => new Vector<T>(source, id * Vector<T>.Count);
        
        static void set<T>(in T[] values, int i, Vector<T> v) where T : struct
            => v.CopyTo(values, i * Vector<T>.Count);
    }
}
