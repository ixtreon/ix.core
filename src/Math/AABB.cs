﻿using System.Diagnostics;
using System.Numerics;

namespace Ix.Math
{
    [DebuggerDisplay("Low = {Low}, High = {High}")]
    public readonly struct AABB
    {
        public readonly Vector2 Low, High;

        public AABB(Vector2 low, Vector2 high)
        {
            Low = low;
            High = high;
        }

        public AABB(in RectangleF bounds)
        {
            Low = bounds.Position;
            High = bounds.FarPosition;
        }

        public AABB Inflate(float value)
        {
            var fat = new Vector2(value);
            return new AABB(Low - fat, High + fat);
        }

        /// <summary>
        /// Gets the union (bounding box) of the 2 areas.
        /// </summary>
        public AABB Union(in AABB other)
        {
            var newLow = Vector2.Min(Low, other.Low);
            var newHigh = Vector2.Min(High, other.High);
            return new AABB(newLow, newHigh);
        }



        public bool FullyContains(in AABB other)
        {
            return Low.X <= other.Low.X
                && Low.Y <= other.Low.Y
                && High.X >= other.High.X
                && High.Y >= other.High.Y;
        }
    }
}
