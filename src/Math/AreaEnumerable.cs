﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Ix.Math
{
    //public struct Point
    //{
    //    public int X, Y;
    //    public Point(int x, int y) => (X, Y) = (x, y);
    //}

    public readonly struct AreaEnumerable : IEnumerable<Point>
    {
        readonly Point startInclusive;
        readonly Point endExclusive;

        public AreaEnumerable(Point startInclusive, Point endExclusive)
        {
            this.startInclusive = startInclusive;
            this.endExclusive = endExclusive;
        }

        public AreaEnumerator GetEnumerator()
            => new AreaEnumerator(startInclusive, endExclusive);

        IEnumerator<Point> IEnumerable<Point>.GetEnumerator()
            => GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator()
            => GetEnumerator();
    }

    /// <summary>
    /// Row-first, exclusive upper bound.
    /// </summary>
    public struct AreaEnumerator : IEnumerator<Point>
    {
        readonly int startX;
        readonly Point max;

        public Point Current { get; private set; }

        public AreaEnumerator(Point startInclusive, Point endExclusive)
        {
            startX = startInclusive.X;
            max = endExclusive;
            Current = new Point(startInclusive.X - 1, startInclusive.Y);
        }

        object IEnumerator.Current => Current;

        public void Reset()
            => throw new NotImplementedException();

        public void Dispose() { }

        public bool MoveNext()
        {
            Current = new Point(Current.X + 1, Current.Y);

            if (Current.X == max.X)
                Current = new Point(startX, Current.Y + 1);

            return Current.Y < max.Y;
        }

    }
}
