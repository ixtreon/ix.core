﻿using System;
using System.Numerics;

namespace Ix.Math
{
    /// <summary>
    /// Represents a point in the 2D plane with integral coordinates. 
    /// </summary>
    public readonly struct Point : IEquatable<Point>
    {
        /// <summary>
        /// The point that has both of its X and Y coordinates set to zero. 
        /// </summary>
        public static readonly Point Zero = new Point();

        /// <summary>
        /// The point that has both of its X and Y coordinates set to one. 
        /// </summary>
        public static readonly Point One = new Point(1);

        public static readonly Point UnitX = new Point(1, 0);
        public static readonly Point UnitY = new Point(0, 1);

        /// <summary>
        /// The X coordinate of the point.
        /// </summary>
        public int X { get; }

        /// <summary>
        /// The Y coordinate of the point.
        /// </summary>
        public int Y { get; }

        /// <summary>
        /// Creates a new point with both its X and Y set to the given value. 
        /// </summary>
        /// <param name="v">The value for both the X and Y values of the point. </param>
        public Point(int v)
        {
            X = Y = v;
        }

        public float Length()
            => (float)System.Math.Sqrt(LengthSquared());

        /// <summary>
        /// Creates a new point with the given X and Y coordinates. 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Point(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }


        public static Point operator -(Point p) 
            => new Point(-p.X, -p.Y);

        public static implicit operator Vector2(Point p)
            => new Vector2(p.X, p.Y);

        public static implicit operator Point((int x, int y) tuple)
            => new Point(tuple.x, tuple.y);

        public Point With(int? x = null, int? y = null)
            => new Point(x ?? X, y ?? Y);

        public void Deconstruct(out int x, out int y) => (x, y) = (X, Y);


        #region Point-point operators

        public static bool operator ==(Point a, Point b) => a.Equals(b);
        public static bool operator !=(Point a, Point b) => !a.Equals(b);

        public static Point operator +(Point a, Point b) => new Point(a.X + b.X, a.Y + b.Y);
        public static Point operator -(Point a, Point b) => new Point(a.X - b.X, a.Y - b.Y);

        public static Point operator *(Point a, Point b) => new Point(a.X * b.X, a.Y * b.Y);
        public static Point operator /(Point a, Point b) => new Point(a.X / b.X, a.Y / b.Y);

        #endregion

        #region Point-int operators
        public static Point operator -(Point a, int other) => new Point(a.X - other, a.Y - other);
        public static Point operator +(Point a, int other) => new Point(a.X + other, a.Y + other);

        public static Point operator *(Point a, int multiplier) => new Point(a.X * multiplier, a.Y * multiplier);
        public static Point operator /(Point a, int divisor) => new Point(a.X / divisor, a.Y / divisor);

        public static Point operator %(Point a, int modulus) => new Point(a.X % modulus, a.Y % modulus);
        #endregion


        public static Point Max(Point a, Point b) => new Point(System.Math.Max(a.X, b.X), System.Math.Max(a.Y, b.Y));

        public static Point Min(Point a, Point b) => new Point(System.Math.Min(a.X, b.X), System.Math.Min(a.Y, b.Y));


        /// <summary>
        /// Returns a point with coordinates clamped 
        /// within the rectangle specified by the given points. 
        /// </summary>
        public Point Clamp(in Point lowLeft, in Point topRight)
        {
            var x = System.Math.Min(topRight.X, System.Math.Max(lowLeft.X, X));
            var y = System.Math.Min(topRight.Y, System.Math.Max(lowLeft.Y, Y));

            return new Point(x, y);
        }

        /// <summary>
        /// Returns a point with coordinates clamped 
        /// within the rectangle specified by the given points. 
        /// </summary>
        public Point Clamp(in Rectangle bounds) => Clamp(bounds.Position, bounds.FarPosition);


        public long LengthSquared() => X * X + Y * Y;

        /// <summary>
        /// Gets the squared Euclidean distance from this point to the given point. 
        /// </summary>
        public long DistanceToSquared(in Point other) => (this - other).LengthSquared();

        /// <summary>
        /// Gets the Euclidean distance from this point to the given point. 
        /// </summary>
        public double DistanceTo(in Point other) => System.Math.Sqrt(DistanceToSquared(other));

        public override bool Equals(object obj) => (obj is Point other) && Equals(other);

        public bool Equals(Point other) => X == other.X && Y == other.Y;

        public override string ToString() => $"[{X}, {Y}]";

        public override int GetHashCode() => (X, Y).GetHashCode();

    }
}
