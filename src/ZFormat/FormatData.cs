﻿using System;
using System.Globalization;

namespace Shanism.Common.ZFormat
{
    readonly ref struct FormatData
    {
        public readonly ReadOnlySpan<char> Format;
        public readonly IFormatProvider FormatProvider;
        public readonly ReadOnlySpan<char> OriginalTemplate;

        public FormatData(ReadOnlySpan<char> format, ReadOnlySpan<char> originalTemplate) : this()
        {
            Format = format;
            OriginalTemplate = originalTemplate;
            FormatProvider = CultureInfo.InvariantCulture;
        }
    }
}