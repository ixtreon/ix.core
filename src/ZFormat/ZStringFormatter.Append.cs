﻿using Ix.Core.Memory;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Shanism.Common.ZFormat
{
    public static partial class ZStringFormatter
    {
		#region AppendLine
		public static void AppendLine<T0>(this RefList<char> destination, T0 arg0)
        {
			destination.Append(arg0);
			destination.Add('\n');
        }
		public static void AppendLine<T0, T1>(this RefList<char> destination, T0 arg0, T1 arg1)
        {
			destination.Append(arg0, arg1);
			destination.Add('\n');
        }
		public static void AppendLine<T0, T1, T2>(this RefList<char> destination, T0 arg0, T1 arg1, T2 arg2)
        {
			destination.Append(arg0, arg1, arg2);
			destination.Add('\n');
        }
		public static void AppendLine<T0, T1, T2, T3>(this RefList<char> destination, T0 arg0, T1 arg1, T2 arg2, T3 arg3)
        {
			destination.Append(arg0, arg1, arg2, arg3);
			destination.Add('\n');
        }
		public static void AppendLine<T0, T1, T2, T3, T4>(this RefList<char> destination, T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
			destination.Append(arg0, arg1, arg2, arg3, arg4);
			destination.Add('\n');
        }
		public static void AppendLine<T0, T1, T2, T3, T4, T5>(this RefList<char> destination, T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
        {
			destination.Append(arg0, arg1, arg2, arg3, arg4, arg5);
			destination.Add('\n');
        }
		public static void AppendLine<T0, T1, T2, T3, T4, T5, T6>(this RefList<char> destination, T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
        {
			destination.Append(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
			destination.Add('\n');
        }
		public static void AppendLine<T0, T1, T2, T3, T4, T5, T6, T7>(this RefList<char> destination, T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7)
        {
			destination.Append(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
			destination.Add('\n');
        }
		#endregion

		#region Append
		public static void Append<T0>(this RefList<char> destination, T0 arg0)
        {
			var formatData = new FormatData(default, default);
			PrimitiveFormatter.Format(arg0, formatData, destination);
        }
		public static void Append<T0, T1>(this RefList<char> destination, T0 arg0, T1 arg1)
        {
			var formatData = new FormatData(default, default);
			PrimitiveFormatter.Format(arg0, formatData, destination);
			PrimitiveFormatter.Format(arg1, formatData, destination);
        }
		public static void Append<T0, T1, T2>(this RefList<char> destination, T0 arg0, T1 arg1, T2 arg2)
        {
			var formatData = new FormatData(default, default);
			PrimitiveFormatter.Format(arg0, formatData, destination);
			PrimitiveFormatter.Format(arg1, formatData, destination);
			PrimitiveFormatter.Format(arg2, formatData, destination);
        }
		public static void Append<T0, T1, T2, T3>(this RefList<char> destination, T0 arg0, T1 arg1, T2 arg2, T3 arg3)
        {
			var formatData = new FormatData(default, default);
			PrimitiveFormatter.Format(arg0, formatData, destination);
			PrimitiveFormatter.Format(arg1, formatData, destination);
			PrimitiveFormatter.Format(arg2, formatData, destination);
			PrimitiveFormatter.Format(arg3, formatData, destination);
        }
		public static void Append<T0, T1, T2, T3, T4>(this RefList<char> destination, T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
			var formatData = new FormatData(default, default);
			PrimitiveFormatter.Format(arg0, formatData, destination);
			PrimitiveFormatter.Format(arg1, formatData, destination);
			PrimitiveFormatter.Format(arg2, formatData, destination);
			PrimitiveFormatter.Format(arg3, formatData, destination);
			PrimitiveFormatter.Format(arg4, formatData, destination);
        }
		public static void Append<T0, T1, T2, T3, T4, T5>(this RefList<char> destination, T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
        {
			var formatData = new FormatData(default, default);
			PrimitiveFormatter.Format(arg0, formatData, destination);
			PrimitiveFormatter.Format(arg1, formatData, destination);
			PrimitiveFormatter.Format(arg2, formatData, destination);
			PrimitiveFormatter.Format(arg3, formatData, destination);
			PrimitiveFormatter.Format(arg4, formatData, destination);
			PrimitiveFormatter.Format(arg5, formatData, destination);
        }
		public static void Append<T0, T1, T2, T3, T4, T5, T6>(this RefList<char> destination, T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
        {
			var formatData = new FormatData(default, default);
			PrimitiveFormatter.Format(arg0, formatData, destination);
			PrimitiveFormatter.Format(arg1, formatData, destination);
			PrimitiveFormatter.Format(arg2, formatData, destination);
			PrimitiveFormatter.Format(arg3, formatData, destination);
			PrimitiveFormatter.Format(arg4, formatData, destination);
			PrimitiveFormatter.Format(arg5, formatData, destination);
			PrimitiveFormatter.Format(arg6, formatData, destination);
        }
		public static void Append<T0, T1, T2, T3, T4, T5, T6, T7>(this RefList<char> destination, T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7)
        {
			var formatData = new FormatData(default, default);
			PrimitiveFormatter.Format(arg0, formatData, destination);
			PrimitiveFormatter.Format(arg1, formatData, destination);
			PrimitiveFormatter.Format(arg2, formatData, destination);
			PrimitiveFormatter.Format(arg3, formatData, destination);
			PrimitiveFormatter.Format(arg4, formatData, destination);
			PrimitiveFormatter.Format(arg5, formatData, destination);
			PrimitiveFormatter.Format(arg6, formatData, destination);
			PrimitiveFormatter.Format(arg7, formatData, destination);
        }
		#endregion

		#region AppendFormat
		public static void AppendFormat<T0>(this RefList<char> destination, string inputString, T0 arg0)
        {
            var input = inputString.AsSpan();
            while (input.Length > 0)
            {
                extractRawAndFormattedParts(ref input, out var rawString, out var fmtString);

                // copy the bare-string
                if (rawString.Length > 0)
                    destination.AddRange(rawString);

                // parse the format-string
                if (fmtString.Length > 0)
                {
                    // extract value being formatted and the format-rule
                    extractFormatParts(fmtString, out var propertyName, out var format);
                    if (propertyName.Length == 1)
                    {
                        switch (propertyName[0] - '0')
                        {
							case 0: 
								PrimitiveFormatter.Format(arg0, new FormatData(format, fmtString), destination);
                                continue;
                        }
                    }

                    // unknown arg - write it verbatim
                    destination.AddRange(fmtString);
                }
            }
        }
		public static void AppendFormat<T0, T1>(this RefList<char> destination, string inputString, T0 arg0, T1 arg1)
        {
            var input = inputString.AsSpan();
            while (input.Length > 0)
            {
                extractRawAndFormattedParts(ref input, out var rawString, out var fmtString);

                // copy the bare-string
                if (rawString.Length > 0)
                    destination.AddRange(rawString);

                // parse the format-string
                if (fmtString.Length > 0)
                {
                    // extract value being formatted and the format-rule
                    extractFormatParts(fmtString, out var propertyName, out var format);
                    if (propertyName.Length == 1)
                    {
                        switch (propertyName[0] - '0')
                        {
							case 0: 
								PrimitiveFormatter.Format(arg0, new FormatData(format, fmtString), destination);
                                continue;
							case 1: 
								PrimitiveFormatter.Format(arg1, new FormatData(format, fmtString), destination);
                                continue;
                        }
                    }

                    // unknown arg - write it verbatim
                    destination.AddRange(fmtString);
                }
            }
        }
		public static void AppendFormat<T0, T1, T2>(this RefList<char> destination, string inputString, T0 arg0, T1 arg1, T2 arg2)
        {
            var input = inputString.AsSpan();
            while (input.Length > 0)
            {
                extractRawAndFormattedParts(ref input, out var rawString, out var fmtString);

                // copy the bare-string
                if (rawString.Length > 0)
                    destination.AddRange(rawString);

                // parse the format-string
                if (fmtString.Length > 0)
                {
                    // extract value being formatted and the format-rule
                    extractFormatParts(fmtString, out var propertyName, out var format);
                    if (propertyName.Length == 1)
                    {
                        switch (propertyName[0] - '0')
                        {
							case 0: 
								PrimitiveFormatter.Format(arg0, new FormatData(format, fmtString), destination);
                                continue;
							case 1: 
								PrimitiveFormatter.Format(arg1, new FormatData(format, fmtString), destination);
                                continue;
							case 2: 
								PrimitiveFormatter.Format(arg2, new FormatData(format, fmtString), destination);
                                continue;
                        }
                    }

                    // unknown arg - write it verbatim
                    destination.AddRange(fmtString);
                }
            }
        }
		public static void AppendFormat<T0, T1, T2, T3>(this RefList<char> destination, string inputString, T0 arg0, T1 arg1, T2 arg2, T3 arg3)
        {
            var input = inputString.AsSpan();
            while (input.Length > 0)
            {
                extractRawAndFormattedParts(ref input, out var rawString, out var fmtString);

                // copy the bare-string
                if (rawString.Length > 0)
                    destination.AddRange(rawString);

                // parse the format-string
                if (fmtString.Length > 0)
                {
                    // extract value being formatted and the format-rule
                    extractFormatParts(fmtString, out var propertyName, out var format);
                    if (propertyName.Length == 1)
                    {
                        switch (propertyName[0] - '0')
                        {
							case 0: 
								PrimitiveFormatter.Format(arg0, new FormatData(format, fmtString), destination);
                                continue;
							case 1: 
								PrimitiveFormatter.Format(arg1, new FormatData(format, fmtString), destination);
                                continue;
							case 2: 
								PrimitiveFormatter.Format(arg2, new FormatData(format, fmtString), destination);
                                continue;
							case 3: 
								PrimitiveFormatter.Format(arg3, new FormatData(format, fmtString), destination);
                                continue;
                        }
                    }

                    // unknown arg - write it verbatim
                    destination.AddRange(fmtString);
                }
            }
        }
		public static void AppendFormat<T0, T1, T2, T3, T4>(this RefList<char> destination, string inputString, T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
            var input = inputString.AsSpan();
            while (input.Length > 0)
            {
                extractRawAndFormattedParts(ref input, out var rawString, out var fmtString);

                // copy the bare-string
                if (rawString.Length > 0)
                    destination.AddRange(rawString);

                // parse the format-string
                if (fmtString.Length > 0)
                {
                    // extract value being formatted and the format-rule
                    extractFormatParts(fmtString, out var propertyName, out var format);
                    if (propertyName.Length == 1)
                    {
                        switch (propertyName[0] - '0')
                        {
							case 0: 
								PrimitiveFormatter.Format(arg0, new FormatData(format, fmtString), destination);
                                continue;
							case 1: 
								PrimitiveFormatter.Format(arg1, new FormatData(format, fmtString), destination);
                                continue;
							case 2: 
								PrimitiveFormatter.Format(arg2, new FormatData(format, fmtString), destination);
                                continue;
							case 3: 
								PrimitiveFormatter.Format(arg3, new FormatData(format, fmtString), destination);
                                continue;
							case 4: 
								PrimitiveFormatter.Format(arg4, new FormatData(format, fmtString), destination);
                                continue;
                        }
                    }

                    // unknown arg - write it verbatim
                    destination.AddRange(fmtString);
                }
            }
        }
		public static void AppendFormat<T0, T1, T2, T3, T4, T5>(this RefList<char> destination, string inputString, T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
        {
            var input = inputString.AsSpan();
            while (input.Length > 0)
            {
                extractRawAndFormattedParts(ref input, out var rawString, out var fmtString);

                // copy the bare-string
                if (rawString.Length > 0)
                    destination.AddRange(rawString);

                // parse the format-string
                if (fmtString.Length > 0)
                {
                    // extract value being formatted and the format-rule
                    extractFormatParts(fmtString, out var propertyName, out var format);
                    if (propertyName.Length == 1)
                    {
                        switch (propertyName[0] - '0')
                        {
							case 0: 
								PrimitiveFormatter.Format(arg0, new FormatData(format, fmtString), destination);
                                continue;
							case 1: 
								PrimitiveFormatter.Format(arg1, new FormatData(format, fmtString), destination);
                                continue;
							case 2: 
								PrimitiveFormatter.Format(arg2, new FormatData(format, fmtString), destination);
                                continue;
							case 3: 
								PrimitiveFormatter.Format(arg3, new FormatData(format, fmtString), destination);
                                continue;
							case 4: 
								PrimitiveFormatter.Format(arg4, new FormatData(format, fmtString), destination);
                                continue;
							case 5: 
								PrimitiveFormatter.Format(arg5, new FormatData(format, fmtString), destination);
                                continue;
                        }
                    }

                    // unknown arg - write it verbatim
                    destination.AddRange(fmtString);
                }
            }
        }
		public static void AppendFormat<T0, T1, T2, T3, T4, T5, T6>(this RefList<char> destination, string inputString, T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
        {
            var input = inputString.AsSpan();
            while (input.Length > 0)
            {
                extractRawAndFormattedParts(ref input, out var rawString, out var fmtString);

                // copy the bare-string
                if (rawString.Length > 0)
                    destination.AddRange(rawString);

                // parse the format-string
                if (fmtString.Length > 0)
                {
                    // extract value being formatted and the format-rule
                    extractFormatParts(fmtString, out var propertyName, out var format);
                    if (propertyName.Length == 1)
                    {
                        switch (propertyName[0] - '0')
                        {
							case 0: 
								PrimitiveFormatter.Format(arg0, new FormatData(format, fmtString), destination);
                                continue;
							case 1: 
								PrimitiveFormatter.Format(arg1, new FormatData(format, fmtString), destination);
                                continue;
							case 2: 
								PrimitiveFormatter.Format(arg2, new FormatData(format, fmtString), destination);
                                continue;
							case 3: 
								PrimitiveFormatter.Format(arg3, new FormatData(format, fmtString), destination);
                                continue;
							case 4: 
								PrimitiveFormatter.Format(arg4, new FormatData(format, fmtString), destination);
                                continue;
							case 5: 
								PrimitiveFormatter.Format(arg5, new FormatData(format, fmtString), destination);
                                continue;
							case 6: 
								PrimitiveFormatter.Format(arg6, new FormatData(format, fmtString), destination);
                                continue;
                        }
                    }

                    // unknown arg - write it verbatim
                    destination.AddRange(fmtString);
                }
            }
        }
		public static void AppendFormat<T0, T1, T2, T3, T4, T5, T6, T7>(this RefList<char> destination, string inputString, T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7)
        {
            var input = inputString.AsSpan();
            while (input.Length > 0)
            {
                extractRawAndFormattedParts(ref input, out var rawString, out var fmtString);

                // copy the bare-string
                if (rawString.Length > 0)
                    destination.AddRange(rawString);

                // parse the format-string
                if (fmtString.Length > 0)
                {
                    // extract value being formatted and the format-rule
                    extractFormatParts(fmtString, out var propertyName, out var format);
                    if (propertyName.Length == 1)
                    {
                        switch (propertyName[0] - '0')
                        {
							case 0: 
								PrimitiveFormatter.Format(arg0, new FormatData(format, fmtString), destination);
                                continue;
							case 1: 
								PrimitiveFormatter.Format(arg1, new FormatData(format, fmtString), destination);
                                continue;
							case 2: 
								PrimitiveFormatter.Format(arg2, new FormatData(format, fmtString), destination);
                                continue;
							case 3: 
								PrimitiveFormatter.Format(arg3, new FormatData(format, fmtString), destination);
                                continue;
							case 4: 
								PrimitiveFormatter.Format(arg4, new FormatData(format, fmtString), destination);
                                continue;
							case 5: 
								PrimitiveFormatter.Format(arg5, new FormatData(format, fmtString), destination);
                                continue;
							case 6: 
								PrimitiveFormatter.Format(arg6, new FormatData(format, fmtString), destination);
                                continue;
							case 7: 
								PrimitiveFormatter.Format(arg7, new FormatData(format, fmtString), destination);
                                continue;
                        }
                    }

                    // unknown arg - write it verbatim
                    destination.AddRange(fmtString);
                }
            }
        }
		#endregion
	}
}