﻿using Ix.Collections;
using Ix.Core.Memory;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace Shanism.Common.ZFormat
{
    static class PropertyFormatter<TOwner>
    {

        delegate void PropertyFormatFunc(TOwner owner, in FormatData format, RefList<char> destination);

        static readonly SortedStringList<PropertyFormatFunc> properties;

        static PropertyFormatter()
        {
            var props = typeof(TOwner).GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static);
            var propList = new List<KeyValuePair<string, PropertyFormatFunc>>();
            foreach (var pi in props)
            {
                var typeCode = Type.GetTypeCode(pi.PropertyType);
                if (typeCode == TypeCode.Object || typeCode == TypeCode.DBNull)
                    continue;

                if (typeCode == TypeCode.Empty)
                    throw new NotImplementedException();

                var owner = Expression.Parameter(typeof(TOwner));
                var format = Expression.Parameter(typeof(FormatData).MakeByRefType());
                var destination = Expression.Parameter(typeof(RefList<char>));

                // owner.PropertyName
                var piGetter = Expression.MakeMemberAccess(owner, pi);

                var underlyingFormatter = PrimitiveFormatter.GetFormatterMI(pi.PropertyType);
                var callFormat = Expression.Call(underlyingFormatter, piGetter, format, destination);

                var theLambda = Expression.Lambda<PropertyFormatFunc>(callFormat, owner, format, destination);
                while (theLambda.CanReduce)
                    theLambda = (Expression<PropertyFormatFunc>)theLambda.ReduceAndCheck();

                propList.Add(new KeyValuePair<string, PropertyFormatFunc>(pi.Name, theLambda.Compile()));
            }

            properties = new SortedStringList<PropertyFormatFunc>(StringComparison.Ordinal, propList);
        }

        public static void Format(TOwner owner, ReadOnlySpan<char> propertyName, in FormatData format, RefList<char> destination)
        {
            if (!properties.TryGetValue(propertyName, out var formatter))
            {
                destination.AddRange(format.OriginalTemplate);
                return;
            }

            formatter(owner, format, destination);
        }
    }
}
