﻿using Ix.Core.Memory;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Shanism.Common.ZFormat
{
    public static partial class ZStringFormatter
    {
		#region SetFormat
		public static void SetFormat<T0>(this RefList<char> destination, string formatString,
            T0 arg0)
        {
			destination.Clear();
			destination.AppendFormat(formatString, arg0);
        }
		public static void SetFormat<T0, T1>(this RefList<char> destination, string formatString,
            T0 arg0, T1 arg1)
        {
			destination.Clear();
			destination.AppendFormat(formatString, arg0, arg1);
        }
		public static void SetFormat<T0, T1, T2>(this RefList<char> destination, string formatString,
            T0 arg0, T1 arg1, T2 arg2)
        {
			destination.Clear();
			destination.AppendFormat(formatString, arg0, arg1, arg2);
        }
		public static void SetFormat<T0, T1, T2, T3>(this RefList<char> destination, string formatString,
            T0 arg0, T1 arg1, T2 arg2, T3 arg3)
        {
			destination.Clear();
			destination.AppendFormat(formatString, arg0, arg1, arg2, arg3);
        }
		public static void SetFormat<T0, T1, T2, T3, T4>(this RefList<char> destination, string formatString,
            T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
			destination.Clear();
			destination.AppendFormat(formatString, arg0, arg1, arg2, arg3, arg4);
        }
		public static void SetFormat<T0, T1, T2, T3, T4, T5>(this RefList<char> destination, string formatString,
            T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
        {
			destination.Clear();
			destination.AppendFormat(formatString, arg0, arg1, arg2, arg3, arg4, arg5);
        }
		public static void SetFormat<T0, T1, T2, T3, T4, T5, T6>(this RefList<char> destination, string formatString,
            T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
        {
			destination.Clear();
			destination.AppendFormat(formatString, arg0, arg1, arg2, arg3, arg4, arg5, arg6);
        }
		public static void SetFormat<T0, T1, T2, T3, T4, T5, T6, T7>(this RefList<char> destination, string formatString,
            T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7)
        {
			destination.Clear();
			destination.AppendFormat(formatString, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
        }
		#endregion

		#region Set
		public static void Set<T0>(this RefList<char> destination, T0 arg0)
        {
			destination.Clear();
			destination.Append(arg0);
        }
		public static void Set<T0, T1>(this RefList<char> destination, T0 arg0, T1 arg1)
        {
			destination.Clear();
			destination.Append(arg0, arg1);
        }
		public static void Set<T0, T1, T2>(this RefList<char> destination, T0 arg0, T1 arg1, T2 arg2)
        {
			destination.Clear();
			destination.Append(arg0, arg1, arg2);
        }
		public static void Set<T0, T1, T2, T3>(this RefList<char> destination, T0 arg0, T1 arg1, T2 arg2, T3 arg3)
        {
			destination.Clear();
			destination.Append(arg0, arg1, arg2, arg3);
        }
		public static void Set<T0, T1, T2, T3, T4>(this RefList<char> destination, T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
			destination.Clear();
			destination.Append(arg0, arg1, arg2, arg3, arg4);
        }
		public static void Set<T0, T1, T2, T3, T4, T5>(this RefList<char> destination, T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
        {
			destination.Clear();
			destination.Append(arg0, arg1, arg2, arg3, arg4, arg5);
        }
		public static void Set<T0, T1, T2, T3, T4, T5, T6>(this RefList<char> destination, T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
        {
			destination.Clear();
			destination.Append(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
        }
		public static void Set<T0, T1, T2, T3, T4, T5, T6, T7>(this RefList<char> destination, T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7)
        {
			destination.Clear();
			destination.Append(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
        }
		#endregion

	}
}