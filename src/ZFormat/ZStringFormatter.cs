﻿using Ix.Core.Memory;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Shanism.Common.ZFormat
{
    public static partial class ZStringFormatter
    {
        public static void AppendLine(this RefList<char> destination)
            => destination.Add('\n');

        public static void AppendFormatObject<T>(this RefList<char> destination, string formatString, T formatWith)
        {
            var input = formatString.AsSpan();
            while (input.Length > 0)
            {
                extractRawAndFormattedParts(ref input, out var rawString, out var fmtString);

                // copy the bare-string
                if (rawString.Length > 0)
                    destination.AddRange(rawString);

                // parse the format-string
                if (fmtString.Length > 0)
                {
                    // extract value being formatted and the format-rule
                    extractFormatParts(fmtString, out var propertyName, out var format);

                    PropertyFormatter<T>.Format(formatWith, propertyName, new FormatData(format, fmtString), destination);
                }
            }
        }

        static void extractRawAndFormattedParts(ref ReadOnlySpan<char> input, out ReadOnlySpan<char> rawString, out ReadOnlySpan<char> fmtString)
        {
            // find length of a bare string that is to be copied
            int rawLength = getRawStringLength(input);

            // if there is a format that follows, find its length too
            if (rawLength < input.Length)
            {
                // find length of fmt-string
                var fmtLength = input.Slice(rawLength).IndexOf('}');

                if (fmtLength >= 0)
                {
                    rawString = input.Slice(0, rawLength);
                    fmtString = input.Slice(rawLength, fmtLength + 1);
                    input = input.Slice(rawLength + fmtLength + 1);
                    return;
                }
            }

            rawString = input;
            fmtString = ReadOnlySpan<char>.Empty;
            input = ReadOnlySpan<char>.Empty;
        }

        static void extractFormatParts(ReadOnlySpan<char> fmtString, out ReadOnlySpan<char> propertyName, out ReadOnlySpan<char> format)
        {
            var fmtContents = fmtString.Slice(1, fmtString.Length - 2);
            var formatDelimiterPos = fmtContents.IndexOf(':');
            if (formatDelimiterPos > 0)
            {
                propertyName = fmtContents.Slice(0, formatDelimiterPos);
                format = fmtContents.Slice(formatDelimiterPos + 1);
            }
            else
            {
                propertyName = fmtContents;
                format = ReadOnlySpan<char>.Empty;
            }
        }

        static int getRawStringLength(ReadOnlySpan<char> input)
        {
            while (true)
            {
                var openingBracePos = input.IndexOf('{');

                // simple string
                if (openingBracePos < 0)
                    return input.Length;

                // a format-string follows
                if (openingBracePos == input.Length - 1 || input[openingBracePos + 1] != '{')
                    return openingBracePos;

                // escaped '{' - repeat the IndexOf search
                input = input.Slice(openingBracePos + 2);
            }
        }
    }
}