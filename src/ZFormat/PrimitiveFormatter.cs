﻿using Ix.Core.Memory;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Shanism.Common.ZFormat
{
    
    static partial class PrimitiveFormatter
    {
        delegate void FormatFunc<T>(T value, in FormatData format, RefList<char> destination);

        static class Cache<T>
        {
            public static FormatFunc<T> Formatter { get; set; }
        }

        static readonly Dictionary<Type, MethodInfo> formatMethodInfos = new Dictionary<Type, MethodInfo>();

        static PrimitiveFormatter()
        {
            AddCustom<bool>(FormatBool);
            AddCustom<char>(FormatChar);
            AddCustom<string>(FormatString);

            AddAllVanillas();
        }

        public static void Format<T>(T value, in FormatData format, RefList<char> destination) 
            => Cache<T>.Formatter(value, format, destination);

        public static MethodInfo GetFormatterMI(Type ty)
            => formatMethodInfos[ty];


        static void AddCustom<T>(FormatFunc<T> formatter)
        {
            Cache<T>.Formatter = formatter;
            formatMethodInfos.Add(typeof(T), formatter.GetMethodInfo());
        }

        static void FormatBool(bool value, in FormatData format, RefList<char> destination)
            => destination.AddRange(value ? bool.TrueString : bool.FalseString);
        static void FormatString(string value, in FormatData format, RefList<char> destination)
            => destination.AddRange(value);
        static void FormatChar(char value, in FormatData format, RefList<char> destination)
            => destination.Add(value);
    }
}
