﻿using System;
using Ix.Core.Memory;

namespace Shanism.Common.ZFormat
{
    static partial class PrimitiveFormatter
    {

		static void AddAllVanillas()
		{
            AddCustom<sbyte>(format_sbyte);
            AddCustom<short>(format_short);
            AddCustom<int>(format_int);
            AddCustom<long>(format_long);
            AddCustom<byte>(format_byte);
            AddCustom<ushort>(format_ushort);
            AddCustom<uint>(format_uint);
            AddCustom<ulong>(format_ulong);
            AddCustom<float>(format_float);
            AddCustom<double>(format_double);
            AddCustom<decimal>(format_decimal);
            AddCustom<DateTime>(format_DateTime);
		}
		

		static void format_sbyte(sbyte value, in FormatData format, RefList<char> destination)
        {
            Span<char> buffer = stackalloc char[512];
            if (value.TryFormat(buffer, out int charsWritten, format.Format, format.FormatProvider))
                destination.AddRange(buffer.Slice(0, charsWritten));
            else
                destination.AddRange(format.OriginalTemplate);
        }

		static void format_short(short value, in FormatData format, RefList<char> destination)
        {
            Span<char> buffer = stackalloc char[512];
            if (value.TryFormat(buffer, out int charsWritten, format.Format, format.FormatProvider))
                destination.AddRange(buffer.Slice(0, charsWritten));
            else
                destination.AddRange(format.OriginalTemplate);
        }

		static void format_int(int value, in FormatData format, RefList<char> destination)
        {
            Span<char> buffer = stackalloc char[512];
            if (value.TryFormat(buffer, out int charsWritten, format.Format, format.FormatProvider))
                destination.AddRange(buffer.Slice(0, charsWritten));
            else
                destination.AddRange(format.OriginalTemplate);
        }

		static void format_long(long value, in FormatData format, RefList<char> destination)
        {
            Span<char> buffer = stackalloc char[512];
            if (value.TryFormat(buffer, out int charsWritten, format.Format, format.FormatProvider))
                destination.AddRange(buffer.Slice(0, charsWritten));
            else
                destination.AddRange(format.OriginalTemplate);
        }

		static void format_byte(byte value, in FormatData format, RefList<char> destination)
        {
            Span<char> buffer = stackalloc char[512];
            if (value.TryFormat(buffer, out int charsWritten, format.Format, format.FormatProvider))
                destination.AddRange(buffer.Slice(0, charsWritten));
            else
                destination.AddRange(format.OriginalTemplate);
        }

		static void format_ushort(ushort value, in FormatData format, RefList<char> destination)
        {
            Span<char> buffer = stackalloc char[512];
            if (value.TryFormat(buffer, out int charsWritten, format.Format, format.FormatProvider))
                destination.AddRange(buffer.Slice(0, charsWritten));
            else
                destination.AddRange(format.OriginalTemplate);
        }

		static void format_uint(uint value, in FormatData format, RefList<char> destination)
        {
            Span<char> buffer = stackalloc char[512];
            if (value.TryFormat(buffer, out int charsWritten, format.Format, format.FormatProvider))
                destination.AddRange(buffer.Slice(0, charsWritten));
            else
                destination.AddRange(format.OriginalTemplate);
        }

		static void format_ulong(ulong value, in FormatData format, RefList<char> destination)
        {
            Span<char> buffer = stackalloc char[512];
            if (value.TryFormat(buffer, out int charsWritten, format.Format, format.FormatProvider))
                destination.AddRange(buffer.Slice(0, charsWritten));
            else
                destination.AddRange(format.OriginalTemplate);
        }

		static void format_float(float value, in FormatData format, RefList<char> destination)
        {
            Span<char> buffer = stackalloc char[512];
            if (value.TryFormat(buffer, out int charsWritten, format.Format, format.FormatProvider))
                destination.AddRange(buffer.Slice(0, charsWritten));
            else
                destination.AddRange(format.OriginalTemplate);
        }

		static void format_double(double value, in FormatData format, RefList<char> destination)
        {
            Span<char> buffer = stackalloc char[512];
            if (value.TryFormat(buffer, out int charsWritten, format.Format, format.FormatProvider))
                destination.AddRange(buffer.Slice(0, charsWritten));
            else
                destination.AddRange(format.OriginalTemplate);
        }

		static void format_decimal(decimal value, in FormatData format, RefList<char> destination)
        {
            Span<char> buffer = stackalloc char[512];
            if (value.TryFormat(buffer, out int charsWritten, format.Format, format.FormatProvider))
                destination.AddRange(buffer.Slice(0, charsWritten));
            else
                destination.AddRange(format.OriginalTemplate);
        }

		static void format_DateTime(DateTime value, in FormatData format, RefList<char> destination)
        {
            Span<char> buffer = stackalloc char[512];
            if (value.TryFormat(buffer, out int charsWritten, format.Format, format.FormatProvider))
                destination.AddRange(buffer.Slice(0, charsWritten));
            else
                destination.AddRange(format.OriginalTemplate);
        }
	}
}