﻿using Ix.Core.Collections;
using Ix.Core.Memory;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Ix.Collections
{
    public interface IReadOnlyStringLookup<T> : IReadOnlyList<KeyValuePair<string, T>>
    {
        new List<KeyValuePair<string, T>>.Enumerator GetEnumerator();

        IIndexable<string> Keys { get; }
        IIndexable<T> Values { get; }
    }


    // TODO: Waiting on `ReadOnlySpan<char>` extension methods for `Dictionary<string>`
    //       see https://github.com/dotnet/runtime/issues/27229 for details
    public class SortedStringList<T> : IReadOnlyStringLookup<T>
    {
        readonly StringComparison comparer;
        readonly List<KeyValuePair<string, T>> storage;

        public IIndexable<string> Keys { get; }
        public IIndexable<T> Values { get; }

        public SortedStringList(StringComparison comparer = StringComparison.Ordinal)
        {
            this.comparer = comparer;

            storage = new List<KeyValuePair<string, T>>();

            Keys = storage.CreateView(i => storage[i].Key);
            Values = storage.CreateView(i => storage[i].Value);
        }

        public SortedStringList(StringComparison comparer, IEnumerable<T> source, Func<T, string> keySelector)
            : this(comparer, source.Select(x => new KeyValuePair<string, T>(keySelector(x), x)))
        {

        }

        public SortedStringList(StringComparison comparer, IEnumerable<KeyValuePair<string, T>> source)
        {
            this.comparer = comparer;

            storage = source.ToList();
            storage.Sort((a, b) => string.Compare(a.Key, b.Key, comparer));

            ThrowIfDuplicatesExist();

            Keys = storage.CreateView(i => storage[i].Key);
            Values = storage.CreateView(i => storage[i].Value);
        }

        void ThrowIfDuplicatesExist()
        {
            for (int i = 1; i < storage.Count; i++)
                if (string.Equals(storage[i - 1].Key, storage[i].Key, comparer))
                    throw new Exception("Duplicate key '" + storage[i].Key + "' found in the source collection");
        }

        public int Count
            => storage.Count;

        public KeyValuePair<string, T> this[int index]
            => storage[index];

        public T this[string key]
        {
            get
            {
                var index = BinarySearch(key);
                if (index < 0)
                    throw new ArgumentException("The provided key was not found in the dictionary", nameof(key));
                return storage[index].Value;
            }
            set
            {
                var index = BinarySearch(key);
                if (index < 0)
                    throw new ArgumentException("The provided key was not found in the dictionary", nameof(key));
                storage[index] = new KeyValuePair<string, T>(key, value);
            }
        }

        /// <summary>
        /// Adds an element to the collection. Throws if the key already exists. 
        /// <para/>
        /// O(n) complexity!
        /// </summary>
        public void Add(string key, T value)
        {
            var id = BinarySearch(key);
            if (id >= 0)
                throw new Exception("Key already exists: " + key);

            id = ~id;
            storage.Insert(id, new KeyValuePair<string, T>(key, value));
        }

        /// <summary>
        /// Adds or replaces an element to the collection. 
        /// <para/>
        /// O(n) complexity!
        /// </summary>
        public void AddOrReplace(string key, T value)
        {
            var id = BinarySearch(key);
            if (id >= 0)
            {
                storage[id] = new KeyValuePair<string, T>(key, value);
            }
            else
            {
                id = ~id;
                storage.Insert(id, new KeyValuePair<string, T>(key, value));
            }
        }

        /// <summary>
        /// Removes a key from the collection.
        /// <para/>
        /// O(n) complexity!
        /// </summary>
        public bool Remove(ReadOnlySpan<char> key)
            => Remove(key, out var _);

        public void Clear()
            => storage.Clear();

        /// <summary>
        /// Removes a key from the collection.
        /// <para/>
        /// O(n) complexity!
        /// </summary>
        public bool Remove(ReadOnlySpan<char> key, out T value)
        {
            var id = BinarySearch(key);
            if (id < 0)
            {
                value = default;
                return false;
            }

            value = storage[id].Value;
            storage.RemoveAt(id);
            return true;
        }

        /// <summary>
        /// Attempts to retrieve the value corresponding to the given <paramref name="key"/>.
        /// <para/>
        /// O(log(N)) complexity!
        /// </summary>
        public bool TryGetValue(ReadOnlySpan<char> key, out T value)
        {
            var id = BinarySearch(key);
            if (id < 0)
            {
                value = default;
                return false;
            }

            value = storage[id].Value;
            return true;
        }

        /// <summary>
        /// Follows the same rules as <see cref="List{T}.BinarySearch(T)"/>. 
        /// </summary>
        public int BinarySearch(ReadOnlySpan<char> value)
            => storage.BinarySearch(value, comparer);


        public List<KeyValuePair<string, T>>.Enumerator GetEnumerator()
            => storage.GetEnumerator();
        IEnumerator<KeyValuePair<string, T>> IEnumerable<KeyValuePair<string, T>>.GetEnumerator()
            => storage.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator()
            => storage.GetEnumerator();
    }
}
