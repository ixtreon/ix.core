﻿using Ix.Core.Memory;
using Ix.Math;
using Ix.Memory;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Numerics;
using System.Runtime.CompilerServices;

namespace Ix.Collections
{

    public class QuadTree<T>
    {

        const int DefaultShrinkCount = 8;
        const int DefaultGrowCount = 16;
        const float DefaultMinimumRange = 0;

        struct QuadItem
        {
            public T UserData;
            public AABB Bounds;
            //public bool ShouldRemove;
        }

        struct QuadNode
        {
            public int ParentID;

            public Vector2 Center;
            public Vector2 Range;

            public bool IsLeaf;
            public int TotalCount;

            public List<int> ItemIDs;

            /// <summary>
            /// One of the 4 leaves.
            /// </summary>
            public int BotLeft, BotRight, TopLeft, TopRight;

            public readonly AABB GetBounds()
            {
                return new AABB(Center - Range, Center + Range);
            }

            /// <summary>
            /// Used for debugging purposes.
            /// </summary>
            public readonly bool ContainsItemID(int itemID)
            {
                for (int i = 0; i < ItemIDs.Count; i++)
                    if (ItemIDs[i] == itemID)
                        return true;
                return false;
            }
        }

        [ThreadStatic]
        static Stack<int> queryStack;


        readonly ObjectPool<List<int>> nodeItemListPool;
        readonly DynamicPool<QuadItem> itemPool;
        readonly DynamicPool<QuadNode> nodePool;
        readonly int rootNodeID;

        public float MinCellSize { get; private set; }

        /// <summary>
        /// That little items within a node causes the node to compact its branches.
        /// </summary>
        public int MaxItemsToShrinkNode { get; private set; }

        /// <summary>
        /// That many items within a node causes the node to grow branches.
        /// </summary>
        public int MinItemsToGrowNode { get; private set; }

        public int Count => nodePool[rootNodeID].TotalCount;

        public QuadTree(RectangleF span,
            float minCellSize = DefaultMinimumRange,
            int maxItemsToShrinkNode = DefaultShrinkCount,
            int minItemsToGrowNode = DefaultGrowCount,
            int initialNodeCapacity = 64)
        {
            if (minItemsToGrowNode < 0)
                throw new ArgumentOutOfRangeException(nameof(minItemsToGrowNode));

            if (minCellSize < 0)
                throw new ArgumentOutOfRangeException(nameof(minCellSize));

            MinCellSize = minCellSize;
            MinItemsToGrowNode = minItemsToGrowNode;
            MaxItemsToShrinkNode = maxItemsToShrinkNode;

            nodeItemListPool = new ObjectPool<List<int>>(initialNodeCapacity);
            nodePool = new DynamicPool<QuadNode>(initialNodeCapacity);
            itemPool = new DynamicPool<QuadItem>(initialNodeCapacity * 4);

            rootNodeID = InitNode(0, span.Center, span.Size / 2);
        }

        [Obsolete("This doesn't do deep clone of QuadNode.ItemIDs.. so will misbehave", true)]
        public void CloneInto(QuadTree<T> destination)
        {
            destination.MinCellSize = MinCellSize;
            destination.MaxItemsToShrinkNode = MaxItemsToShrinkNode;
            destination.MinItemsToGrowNode = MinItemsToGrowNode;

            PoolMarshal.Clone(itemPool, destination.itemPool);
            PoolMarshal.Clone(nodePool, destination.nodePool);
        }

        public int AssertDebugCount()
            => assertDebugCount(0);

        int assertDebugCount(int id)
        {
            var manualCount = nodePool[id].ItemIDs.Count;
            if (!nodePool[id].IsLeaf)
                manualCount = manualCount
                + assertDebugCount(nodePool[id].TopLeft)
                + assertDebugCount(nodePool[id].TopRight)
                + assertDebugCount(nodePool[id].BotLeft)
                + assertDebugCount(nodePool[id].BotRight);

            if (nodePool[id].TotalCount != manualCount)
                throw new Exception();

            return manualCount;
        }

        public bool Verify(int nodeID)
        {
            ref var n = ref nodePool[nodeID];
            var nBounds = n.GetBounds();

            if (n.IsLeaf)
            {
                if (nodeID == rootNodeID)
                    return true;

                foreach (var childItem in n.ItemIDs)
                    if (!nBounds.FullyContains(itemPool[childItem].Bounds))
                        return false;

                return true;
            }
            else
            {
                foreach (var childItem in n.ItemIDs)
                    if (GetOwningLeaf(nodeID, itemPool[childItem].Bounds) != nodeID)
                        return false;

                return Verify(n.BotLeft) && Verify(n.BotRight)
                    && Verify(n.TopLeft) && Verify(n.TopRight);

            }
        }


        public int Add(T userData, in RectangleF bounds)
        {
            // init item
            var itemID = itemPool.New();
            itemPool[itemID].UserData = userData;
            itemPool[itemID].Bounds = new AABB(bounds.Position, bounds.FarPosition);

            AddToTree(rootNodeID, itemID);

            return itemID;
        }

        public void Move(int itemID, in RectangleF bounds)
            => Move(itemID, new AABB(bounds.Position, bounds.FarPosition));


        public void Move(int itemID, in AABB newBounds)
        {
            // descend, as long as both owned by same parent
            var currentNode = rootNodeID;
            var itemBounds = itemPool[itemID].Bounds;
            while (!nodePool[currentNode].IsLeaf)
            {
                var oldParent = GetOwningLeaf(currentNode, itemBounds);
                var newParent = GetOwningLeaf(currentNode, newBounds);
                if (oldParent != newParent)
                {
                    // move from one sub-branch to another
                    if (oldParent == currentNode)
                        nodePool[currentNode].ItemIDs.Remove(itemID);
                    else
                        RemoveFromTree(oldParent, itemID);

                    itemPool[itemID].Bounds = newBounds;

                    if (newParent == currentNode)
                        nodePool[currentNode].ItemIDs.Add(itemID);
                    else
                        AddToTree(newParent, itemID);

                    return;
                }
                else if (oldParent == currentNode)
                    break;  // was here, remains here...

                currentNode = newParent;
            }

            // OK, both are in the same leaf - just change their bounds!
            itemPool[itemID].Bounds = newBounds;
        }

        public void Remove(int itemID)
            => Remove(itemID, out var _);

        public void Remove(int itemID, out T userData)
        {
            RemoveFromTree(rootNodeID, itemID);

            userData = itemPool[itemID].UserData;
            itemPool[itemID].UserData = default;
            itemPool.Release(itemID);
        }

        /// <summary>
        /// Note: does not de-allocate the item!
        /// </summary>
        void RemoveFromTree(int nodeID, int itemID)
        {
            nodePool[nodeID].TotalCount--;

            // find the parent - either a leaf (if fully contained)
            // or compound (if on the border between two nodes)
            var itemBounds = itemPool[itemID].Bounds;
            while (!nodePool[nodeID].IsLeaf)
            {
                var owningChildID = GetOwningLeaf(nodeID, itemBounds);
                if (owningChildID == nodeID)
                    break;

                nodeID = owningChildID;
                nodePool[nodeID].TotalCount--;
            }

            // try remove from current node
            var nodeList = nodePool[nodeID].ItemIDs;
            var itemIndexInNodeList = nodeList.IndexOf(itemID);
            Debug.Assert(itemIndexInNodeList >= 0);

            // remove from node's list - node count already up-to-date
            nodeList.RemoveAtFast(itemIndexInNodeList);

            TryCompactNode(nodeID);
        }


        void AddToTree(int nodeID, int itemID)
        {
            nodePool[nodeID].TotalCount++;

            // retrieve the correct destination branch, add +1 to obj. counts while descending
            var itemBounds = itemPool[itemID].Bounds;
            while (!nodePool[nodeID].IsLeaf)
            {
                var owningChildID = GetOwningLeaf(nodeID, itemBounds);
                Debug.Assert(nodeID == 0 || owningChildID != rootNodeID);

                if (owningChildID == nodeID)
                    break;

                nodeID = owningChildID;
                nodePool[nodeID].TotalCount++;
            }

            // add to the leaf
            nodePool[nodeID].ItemIDs.Add(itemID);

            // make branches? othewise we're done
            TryExpandNode(nodeID);
        }

        public int QueryRect(in RectangleF query, IList<T> results)
            => QueryRect(new AABB(query), results);

        public int QueryRect(in AABB query, IList<T> results)
            => QueryRect<T>(query, results);

        public int QueryRect<TQuery>(in RectangleF query, IList<TQuery> results)
            where TQuery : T
            => QueryRect(new AABB(query), results);

        public int QueryRect<TQuery>(in AABB query, IList<TQuery> results)
            where TQuery : T
        {
            if (!intersects(nodePool[rootNodeID].GetBounds(), query))
                return 0;

            if (queryStack == null)
                queryStack = new Stack<int>();


            var count = 0;
            queryStack.Push(rootNodeID);

            do
            {
                var curNodeID = queryStack.Pop();
                ref var curNode = ref nodePool[curNodeID];

                // get items from this node
                var curItemIDs = curNode.ItemIDs;
                for (int i = 0; i < curItemIDs.Count; i++)
                {
                    var item = itemPool[curItemIDs[i]];
                    if (item.UserData is TQuery data && intersects(query, item.Bounds))
                    {
                        count++;
                        results.Add(data);
                    }
                }
                // leaf -> we're done
                if (curNode.IsLeaf)
                    continue;

                // see which of the 4 quadrants the rect intersects
                var split = curNode.Center;

                var l = query.Low.X < split.X;
                var r = query.High.X > split.X;
                var t = query.Low.Y < split.Y;
                var b = query.High.Y > split.Y;

                if (l)
                {
                    if (b) queryStack.Push(curNode.BotLeft);
                    if (t) queryStack.Push(curNode.TopLeft);
                }

                if (r)
                {
                    if (b) queryStack.Push(curNode.BotRight);
                    if (t) queryStack.Push(curNode.TopRight);
                }
            }
            while (queryStack.Count > 0);

            return count;
        }

        public int QueryEllipseFast(in Ellipse query, IList<T> results, Func<T, RectangleF> getBounds)
        {
            var nItemsAdded = QueryRect(query.GetBoundingBox(), results);
            var start = results.Count - nItemsAdded;
            for (int i = 0; i < results.Count; i++)
                if (!getBounds(results[i]).Intersects(query))
                {
                    results.RemoveAtFast(i);
                    i--;
                }
            return results.Count - start;
        }

        int GetOwningLeaf(int parentNodeID, in AABB bounds)
        {
            var split = nodePool[parentNodeID].Center;

            var l = bounds.Low.X < split.X;
            var r = bounds.High.X > split.X;
            var t = bounds.Low.Y < split.Y;
            var b = bounds.High.Y > split.Y;

            if ((l && r) || (t && b))
                return parentNodeID;

            if (r)
            {
                if (b) return nodePool[parentNodeID].BotRight;
                else return nodePool[parentNodeID].TopRight;
            }
            else
            {
                if (b) return nodePool[parentNodeID].BotLeft;
                else return nodePool[parentNodeID].TopLeft;
            }
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        bool intersects(in AABB a, in AABB b)
            => b.High.X > a.Low.X
            && b.High.Y > a.Low.Y
            && a.High.X > b.Low.X
            && a.High.Y > b.Low.Y;



        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        bool TryExpandNode(int nodeID)
        {
            if (!nodePool[nodeID].IsLeaf || nodePool[nodeID].ItemIDs.Count < MinItemsToGrowNode || !CanSplit(nodeID))
                return false;

            nodePool[nodeID].IsLeaf = false;
            ExpandNode_InitBranches(nodeID);
            ExpandNode_MoveChildren(nodeID);

            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        bool TryCompactNode(int nodeID)
        {
            int timesCompacted = 0;
            while (!nodePool[nodeID].IsLeaf && nodePool[nodeID].TotalCount < MaxItemsToShrinkNode)
            {
                Debug.Assert(nodePool[nodePool[nodeID].BotLeft].IsLeaf);
                Debug.Assert(nodePool[nodePool[nodeID].BotRight].IsLeaf);
                Debug.Assert(nodePool[nodePool[nodeID].TopLeft].IsLeaf);
                Debug.Assert(nodePool[nodePool[nodeID].TopRight].IsLeaf);

                nodePool[nodeID].IsLeaf = true;

                var nodeItems = nodePool[nodeID].ItemIDs;

                CompactBranch(nodeItems, nodePool[nodeID].BotLeft);
                CompactBranch(nodeItems, nodePool[nodeID].BotRight);
                CompactBranch(nodeItems, nodePool[nodeID].TopLeft);
                CompactBranch(nodeItems, nodePool[nodeID].TopRight);

                nodeID = nodePool[nodeID].ParentID;
                timesCompacted++;
            }

            return timesCompacted > 0;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void CompactBranch(List<int> dest, int fromNodeID)
        {
            // move the items to parent
            var itemList = nodePool[fromNodeID].ItemIDs;
            dest.AddRange(itemList);

            // return the child-item list
            itemList.Clear();
            nodeItemListPool.Return(itemList);

            // return the node
            nodePool.Release(fromNodeID);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void ExpandNode_InitBranches(int nodeID)
        {
            var origin = nodePool[nodeID].Center;
            var radius = nodePool[nodeID].Range / 2;
            var min = origin - radius;
            var max = origin + radius;

            nodePool[nodeID].TopLeft = InitNode(nodeID, new Vector2(min.X, min.Y), radius);
            nodePool[nodeID].TopRight = InitNode(nodeID, new Vector2(max.X, min.Y), radius);
            nodePool[nodeID].BotLeft = InitNode(nodeID, new Vector2(min.X, max.Y), radius);
            int botRight = InitNode(nodeID, new Vector2(max.X, max.Y), radius);
            nodePool[nodeID].BotRight = botRight;

            Debug.Assert(nodePool[nodeID].TopLeft != 0);
            Debug.Assert(nodePool[nodeID].TopRight != 0);
            Debug.Assert(nodePool[nodeID].BotLeft != 0);
            Debug.Assert(nodePool[nodeID].BotRight != 0);
        }

        int InitNode(in int parentID, in Vector2 center, in Vector2 range)
        {
            var id = nodePool.New();
            ref var node = ref nodePool[id];

            node.ParentID = parentID;
            node.Center = center;
            node.Range = range;
            node.IsLeaf = true;
            node.TotalCount = 0;

            node.ItemIDs = nodeItemListPool.Get();

            return id;
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void ExpandNode_MoveChildren(int nodeID)
        {
            var nodeItems = nodePool[nodeID].ItemIDs;
            var lastItemID = nodeItems.Count - 1;
            for (int i = lastItemID; i >= 0; i--)
            {
                var itemID = nodeItems[i];
                var itemBounds = itemPool[itemID].Bounds;

                var newOwner = GetOwningLeaf(nodeID, itemBounds);
                if (newOwner == nodeID)
                    continue;

                // move to new owner (a kid of ours)
                nodePool[newOwner].ItemIDs.Add(nodeItems[i]);
                nodePool[newOwner].TotalCount++;

                // remove from us
                nodeItems[i] = nodeItems[lastItemID];
                nodeItems.RemoveAt(lastItemID);
                lastItemID--;
            }
        }

        /// <summary>
        /// Returns whether the node with the given ID is big enough to be further split. 
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        bool CanSplit(int id)
            => nodePool[id].Range.X > 2 * MinCellSize
            && nodePool[id].Range.Y > 2 * MinCellSize;

    }
}
