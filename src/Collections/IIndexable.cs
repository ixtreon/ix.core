﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Ix.Core.Collections
{
    /// <summary>
    /// Similar to <see cref="ReadOnlySpan{T}"/> but is an interface. 
    /// Should have a zero-cost enumerator.
    /// </summary>
    public interface IIndexable<T> : IEnumerable<T>
    {
        int Count { get; }
        T this[int index] { get; }


        public new Enumerator GetEnumerator() => new Enumerator(this);
        IEnumerator<T> IEnumerable<T>.GetEnumerator() => new Enumerator(this);
        IEnumerator IEnumerable.GetEnumerator() => new Enumerator(this);


        public struct Enumerator : IEnumerator<T>
        {
            readonly IIndexable<T> @base;
            int index;

            public Enumerator(IIndexable<T> @base) : this()
            {
                this.@base = @base;
                index = -1;
            }

            public T Current => @base[index];
            object IEnumerator.Current => Current;
            public void Dispose() { }
            public void Reset() => index = -1;
            public bool MoveNext() => ++index < @base.Count;
        }
    }

    public interface IRefIndexable<T> : IEnumerable<T>
    {
        int Count { get; }
        ref T this[int index] { get; }


        public new Enumerator GetEnumerator() => new Enumerator(this);
        IEnumerator<T> IEnumerable<T>.GetEnumerator() => new Enumerator(this);
        IEnumerator IEnumerable.GetEnumerator() => new Enumerator(this);


        public struct Enumerator : IEnumerator<T>
        {
            readonly IRefIndexable<T> @base;
            int index;

            public Enumerator(IRefIndexable<T> @base) : this()
            {
                this.@base = @base;
                index = -1;
            }

            public T Current => @base[index];
            object IEnumerator.Current => Current;
            public void Dispose() { }
            public void Reset() => index = -1;
            public bool MoveNext() => ++index < @base.Count;
        }
    }
}
