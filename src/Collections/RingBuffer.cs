﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ix.Core.Collections
{
    public class RingBuffer<T>
    {

        readonly T[] storage;

        int first;      // inclusive
        int count;

        public int Count => count;
        public bool IsEmpty => count == 0;
        public bool IsFull => count == storage.Length;

        public RingBuffer(int capacity)
        {
            storage = new T[capacity];
            first = count = 0;
        }

        public RingBuffer(int capacity, IEnumerable<T> values) : this(capacity)
        {
            foreach (var val in values)
            {
                storage[count] = val;
                if (++count == capacity)
                    break;
            }
        }

        public IEnumerable<T> EnumerateItems()
        {
            GetSpans(out var a, out var b);
            var maxI = storage.Length - first;
            for (int i = 0; i < count; i++)
            {
                var index = (i < maxI) ? first + i : i - maxI;
                yield return storage[index];
            }
        }

        public ref T this[int index] => ref Get(index);

        public ref T Get(int index)
        {
            if (index < 0 || index >= count)
                throw new IndexOutOfRangeException();

            var storageIndex = first + index;
            if (storageIndex > storage.Length)
                return ref storage[storageIndex - storage.Length];
            return ref storage[storageIndex];
        }

        public ref T First()
        {
            if (count == 0) throw new InvalidOperationException();
            return ref storage[first];
        }

        public ref T Last()
        {
            if (count == 0) throw new InvalidOperationException();
            var i = first + count - 1;
            if (i < storage.Length)
                return ref storage[i];
            return ref storage[i - storage.Length];
        }


        public void GetSpans(out ReadOnlySpan<T> firstSpan, out ReadOnlySpan<T> secondSpan)
        {
            var overreach = first + count - storage.Length;
            if (overreach <= 0)
            {
                firstSpan = storage.AsSpan(first, count);
                secondSpan = default;
            }
            else
            {
                firstSpan = storage.AsSpan(first, storage.Length - first);
                secondSpan = storage.AsSpan(0, overreach);
            }
        }

        public void AddFirst(in T item)
        {
            // move first + update count (unless max)
            moveHeadBackwards();
            if (count < storage.Length)
                count++;

            storage[first] = item;
        }

        public void AddLast(in T item)
        {
            storage[first + count] = item;

            // update count or overwrite/move first
            if (count < storage.Length)
                count++;
            else
                moveHeadForwards();
        }

        public T RemoveFirst()
        {
            if (count == 0) throw new InvalidOperationException($"The collection is empty.");

            // get the item, clear the storage
            var value = storage[first];
            storage[first] = default;

            // update first
            moveHeadForwards();

            count--;
            return value;
        }

        public T RemoveLast()
        {
            if (count == 0) throw new InvalidOperationException($"The collection is empty.");

            // update count
            count--;

            // get the index of the item
            var index = first + count;
            if (index > storage.Length)
                index -= storage.Length;

            // get the item, clear the storage
            var value = storage[index];
            storage[index] = default;

            return value;
        }


        void moveHeadForwards()
        {
            if (++first == storage.Length)
                first = 0;
        }
        void moveHeadBackwards()
        {
            if (--first < 0)
                first = storage.Length - 1;
        }

    }
}
