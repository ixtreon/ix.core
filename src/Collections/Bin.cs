﻿using Ix.Math;
using Ix.Memory;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Numerics;

namespace Ix.Collections
{

    /// <summary>
    /// A 2-dimensional bin data structure.
    /// </summary>
    public class Bin
    {

        // grid bounds
        public RectangleF Bounds { get; }

        /// <summary>
        /// The number of individual bins along X, Y within this structure.
        /// </summary>
        public Point GridSize { get; }

        /// <summary>
        /// The size of each individual bin within this structure.
        /// </summary>
        public Vector2 BinSize { get; }

        //readonly Vector2 cellSize;

        DynamicPool<RectangleF> objects;
        readonly List<int>[,] grid;
        readonly List<int> outside = new List<int>();
        readonly HashSet<(int, int)> collisions = new HashSet<(int, int)>();

        public int CellCount => objects.Count;

        public Bin(RectangleF bounds, Point gridSize, int initialCapacity = 1024)
        {
            objects = new DynamicPool<RectangleF>(initialCapacity);

            Bounds = bounds;
            GridSize = gridSize;
            BinSize = bounds.Size / this.GridSize;

            grid = new List<int>[gridSize.X, gridSize.Y];
            for (int x = 0; x < gridSize.X; x++)
                for (int y = 0; y < gridSize.Y; y++)
                    grid[x, y] = new List<int>();
        }


        public int Add(in RectangleF objBounds)
        {
            var id = objects.New();

            objects[id] = objBounds;
            AddToList(objBounds, id);

            return id;
        }


        public void Move(int id, in RectangleF objBounds)
        {
            RemoveFromList(objects[id], id);

            objects[id] = objBounds;
            AddToList(objBounds, id);
        }

        public void Remove(int id)
        {
            RemoveFromList(objects[id], id);
            objects.Release(id);
        }

        Point GetLowPos(in RectangleF objBounds)
            => ((objBounds.Position - Bounds.Position) / BinSize).Floor();
        Point GetHighPos(in RectangleF objBounds)
            => ((objBounds.FarPosition - Bounds.Position) / BinSize).Ceiling();

        void RemoveFromList(in RectangleF objBounds, int id)
        {
            var (from, to, isOutside) = GetFromToEtc(objBounds);

            for (int x = from.X; x <= to.X; x++)
                for (int y = from.Y; y <= to.Y; y++)
                {
                    grid[x, y].Remove(id);
                    Debug.Assert(!grid[x, y].Contains(id));
                }


            if (isOutside)
            {
                outside.Remove(id);
            }
        }

        int NBinsWithPts(int nPoints)
        {
            var withAnything = 0;
            for (int x = 0; x < GridSize.X - 1; x++)
                for (int y = 0; y < GridSize.Y - 1; y++)
                    if (grid[x, y].Count == nPoints)
                        withAnything++;
            return withAnything;
        }

        public RectangleF GetSavedPosition(int id) => objects[id];

        void AddToList(in RectangleF objBounds, int id)
        {
            var (from, to, isOutside) = GetFromToEtc(objBounds);

            for (int x = from.X; x <= to.X; x++)
                for (int y = from.Y; y <= to.Y; y++)
                {
                    Debug.Assert(!grid[x, y].Contains(id));
                    grid[x, y].Add(id);
                }

            if (isOutside)
            {
                outside.Add(id);
            }
        }


        public int CheckCollisions(Action<int, int> handler)
        {
            collisions.Clear();
            var nCollisions = 0;

            // check each cell in the grid..
            for (int x = 0; x < GridSize.X; x++)
                for (int y = 0; y < GridSize.Y; y++)
                    CheckBinCollisions(grid[x, y], handler, ref nCollisions);

            // and the outside fuckers, too
            CheckBinCollisions(outside, handler, ref nCollisions);

            return nCollisions;
        }

        void CheckBinCollisions(in List<int> cell, in Action<int, int> handler, ref int nCollisions)
        {
            for (int i = 0; i < cell.Count; i++)
                for (int j = i + 1; j < cell.Count; j++)
                {
                    var first = cell[i];
                    var second = cell[j];

                    // do a manual check
                    if (!objects[first].Intersects(objects[second]))
                        continue;

                    // return at most once
                    var pair = (first, second);
                    if (!collisions.Add(pair))
                        continue;

                    nCollisions++;
                    handler(first, second);
                }
        }

        (Point from, Point to, bool isOutside) GetFromToEtc(in RectangleF objBounds)
        {
            var from = GetLowPos(objBounds);
            var to = GetHighPos(objBounds);

            var isOutside
                = from.X < 0 || to.X >= GridSize.X
                || from.Y < 0 || from.Y >= GridSize.Y;

            from = Point.Max(Point.Zero, from);
            to = Point.Min(to, GridSize - 1);

            return (from, to, isOutside);
        }
    }
}
