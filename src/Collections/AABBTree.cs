﻿using Ix.Math;
using Ix.Memory;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Numerics;
using System.Runtime.CompilerServices;

namespace Ix.Collections
{

    public class AABBTree<T>
    {

        const int NullNodeID = -1;


        struct AABBNode
        {
            public bool IsLeaf => Left == NullNodeID;

            public AABB AABB;

            public int Parent;

            public int Left;
            public int Right;

            public T UserData;

            public int Height;
        }

        // cache vars
        readonly DynamicPool<AABBNode> nodes;

        int rootNode = NullNodeID;


        public AABBTree(int initialCapacity = 8)
        {
            nodes = new DynamicPool<AABBNode>(initialCapacity);
        }

        public float FattenValue { get; } = 1;

        public int Insert(in RectangleF rect, T e)
        {
            // create the new node
            var leafID = allocateLeaf();
            nodes[leafID].AABB = new AABB(rect).Inflate(FattenValue);
            nodes[leafID].UserData = e;

            insert(leafID);

            return leafID;
        }

        void insert(int leafID)
        {
            if (rootNode == NullNodeID)
            {
                rootNode = leafID;
                nodes[leafID].Parent = NullNodeID;
                return;
            }

            // find the best sibling for the node
            var sibling = findSiblingToReplace(leafID);

            // create a new parent
            // for the just-inserted-node and its best-match-sibling
            var oldParent = nodes[sibling].Parent;
            var newParent = nodes.New();
            nodes[newParent].UserData = default;
            nodes[newParent].Parent = oldParent;
            nodes[newParent].AABB = nodes[leafID].AABB.Union(nodes[sibling].AABB);
            nodes[newParent].Height = nodes[sibling].Height + 1;
            nodes[newParent].Left = sibling;
            nodes[newParent].Right = leafID;

            nodes[sibling].Parent = newParent;
            nodes[leafID].Parent = newParent;

            if (oldParent == NullNodeID)
            {
                // replacing the root -> now it's us!
                rootNode = newParent;
            }
            else
            {
                // replacing others -> link to us
                if (sibling == nodes[oldParent].Left)
                    nodes[oldParent].Left = newParent;
                else
                    nodes[oldParent].Right = newParent;
            }

            // walk back up the tree, fixing height + AABB
            balanceRecursive(nodes[leafID].Parent);
        }

        public void Remove(int id)
        {
            Debug.Assert(nodes[id].IsLeaf);

            remove(id);
            nodes.Release(id);
        }

        public void Move(int id, in RectangleF bounds, Vector2 velocity)
        {
            Debug.Assert(nodes[id].IsLeaf);

            var aabb = new AABB(bounds);
            if (nodes[id].AABB.FullyContains(aabb))
                return;

            remove(id);

            // add fatten + velocity
            aabb = aabb.Inflate(FattenValue);
            Vector2 pushMin = default, pushMax = default;
            if (velocity.X < 0)
                pushMin.X = velocity.X;
            else
                pushMax.X = velocity.X;
            if (velocity.Y < 0)
                pushMin.Y = velocity.Y;
            else
                pushMax.Y = velocity.Y;
            aabb = new AABB(aabb.Low + pushMin, aabb.High + pushMax);

            nodes[id].AABB = aabb;
            insert(id);
        }

        void remove(int leafID)
        {
            if (leafID == rootNode)
            {
                rootNode = NullNodeID;
                return;
            }

            var parentID = nodes[leafID].Parent;
            ref var parentNode = ref nodes[parentID];
            var sibling = parentNode.Left == leafID ? parentNode.Right : parentNode.Left;
            var gpID = parentNode.Parent;

            if (gpID == NullNodeID)
            {
                // place `sibling` at the root
                rootNode = sibling;
                nodes[sibling].Parent = NullNodeID;

                nodes.Release(parentID);
                return;
            }

            // remove `parent`, connect `sibling` in its place
            if (nodes[gpID].Left == parentID)
                nodes[gpID].Left = sibling;
            else
                nodes[gpID].Right = sibling;
            nodes[sibling].Parent = gpID;

            nodes.Release(parentID);

            // rebalance
            balanceRecursive(gpID);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        int findSiblingToReplace(int leafID)
        {
            var leafRect = nodes[leafID].AABB;
            var current = rootNode;
            while (!nodes[current].IsLeaf)
            {
                var currentCost = Cost(nodes[current].AABB);

                var child1 = nodes[current].Left;
                var child2 = nodes[current].Right;

                // cost if "current" is replaced with a new parent:
                var aabbIfAddedHere = leafRect.Union(nodes[current].AABB);
                var costIfAddedHere = Cost(aabbIfAddedHere);

                // cost for creating a parent for cur + leaf
                var thisCost = 2 * costIfAddedHere;

                // min cost for pushing down the tree
                var inheritCost = 2 * (costIfAddedHere - currentCost);

                //cost of children
                var cost1 = inheritCost + childCost(child1, leafRect);
                var cost2 = inheritCost + childCost(child2, leafRect);

                if (thisCost < cost1 && thisCost < cost2)
                    return current;  // this is the best node

                // some child is the best node
                current = cost1 < cost2 ? child1 : child2;
            }

            return current;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void balanceRecursive(int current)
        {
            do
            {
                current = balanceOne(current);

                Debug.Assert(nodes[current].Left != NullNodeID);
                Debug.Assert(nodes[current].Right != NullNodeID);

                current = nodes[current].Parent;
            }
            while (current != NullNodeID);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        int balanceOne(int index)
        {
            index = balanceInner(index);

            ref var left = ref nodes[index].Left;
            ref var right = ref nodes[index].Right;

            nodes[index].Height = System.Math.Max(nodes[left].Height, nodes[right].Height) + 1;
            nodes[index].AABB = nodes[left].AABB.Union(nodes[right].AABB);

            return index;
        }

        int balanceInner(int iA)
        {
            if (nodes[iA].IsLeaf || nodes[iA].Height < 2)
                return iA;

            /* Basic Configuration:
             *      |
             *      A
             *    /   \
             *   B     C
             *  / \   / \
             * D   E F   G
             */

            var iB = nodes[iA].Left;
            var iC = nodes[iA].Right;
            var curBalance = nodes[iC].Height - nodes[iB].Height;
            var aParent = nodes[iA].Parent;

            // rotate C up
            if (curBalance > 1)
            {
                /*     |
                 *     C
                 *    / 
                 *   A  
                 *  / 
                 * B  
                 */

                var iF = nodes[iC].Left;
                var iG = nodes[iC].Right;

                setLeftChild(iC, iA);
                setParent(iC, aParent, iA);

                // rotate
                if (nodes[iF].Height > nodes[iG].Height)
                {

                    /*      |
                     *      C
                     *    /   \
                     *   A     F
                     *  / \
                     * B   G
                     */

                    setRightChild(iC, iF);
                    setRightChild(iA, iG);

                    updateAABB(iA, iB, iG);
                    updateAABB(iC, iA, iF);
                }
                else
                {

                    /* 
                     *      C
                     *    /   \
                     *   A     G
                     *  / \
                     * B   F
                     */

                    setRightChild(iC, iG);
                    setRightChild(iA, iF);

                    updateAABB(iA, iB, iF);
                    updateAABB(iC, iA, iG);
                }

                return iC;
            }

            // rotate B up
            if (curBalance < -1)
            {
                /*     |
                 *     B
                 *    / 
                 *   A  
                 *    \
                 *     C
                 */

                var iD = nodes[iB].Left;
                var iE = nodes[iB].Right;

                setLeftChild(iB, iA);
                setParent(iB, aParent, iA);

                // rotate
                if (nodes[iD].Height > nodes[iE].Height)
                {
                    /*     |
                     *     B
                     *    / \ 
                     *   A   D
                     *  / \
                     * E   C
                     */
                    setLeftChild(iA, iE);
                    setRightChild(iB, iD);

                    updateAABB(iA, iE, iC);
                    updateAABB(iB, iA, iD);
                }
                else
                {
                    /*     |
                     *     B
                     *    / \ 
                     *   A   E
                     *  / \
                     * D   C
                     */
                    setLeftChild(iA, iD);
                    setRightChild(iB, iE);

                    updateAABB(iA, iD, iC);
                    updateAABB(iB, iA, iE);

                }

                return iB;
            }

            return iA;
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void updateAABB(int node, int left, int right)
        {
            nodes[node].AABB = nodes[left].AABB.Union(nodes[right].AABB);
            nodes[node].Height = 1 + System.Math.Max(nodes[left].Height, nodes[right].Height);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void setLeftChild(int node, int child)
        {
            nodes[node].Left = child;
            nodes[child].Parent = node;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void setRightChild(int node, int child)
        {
            nodes[node].Right = child;
            nodes[child].Parent = node;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void setParent(int n, int parent, int oldParentsChild)
        {
            nodes[n].Parent = parent;
            if (parent == NullNodeID)
            {
                rootNode = n;
            }
            else
            {
                if (nodes[parent].Left == oldParentsChild)
                    nodes[parent].Left = n;
                else
                    nodes[parent].Right = n;
            }
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        float childCost(int childId, in AABB r)
        {
            if (nodes[childId].IsLeaf)
            {
                var combinedRect = r.Union(nodes[childId].AABB);
                return Cost(combinedRect);
            }
            else
            {
                var combinedRect = r.Union(nodes[childId].AABB);
                var oldCost = Cost(nodes[childId].AABB);
                var newCost = Cost(combinedRect);
                return newCost - oldCost;
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        int allocateLeaf()
        {
            var id = nodes.New();

            nodes[id].Parent = NullNodeID;
            nodes[id].Left = NullNodeID;
            nodes[id].Right = NullNodeID;
            nodes[id].AABB = default;
            nodes[id].UserData = default;
            nodes[id].Height = -1;

            return id;
        }

        static float Cost(in AABB r)
        {
            var d = r.High - r.Low;
            return d.X + d.Y;
        }
    }
}
